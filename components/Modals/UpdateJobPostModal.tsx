import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/react';
import { useDisclosure } from '@chakra-ui/hooks';
import { Button } from '@chakra-ui/button';
import { VStack } from '@chakra-ui/layout';
import { Flex } from '@chakra-ui/react';
import {
  TextInput,
  CheckboxInput,
  NumberInputs,
  TextAreaInput,
  RadioButtonInput,
} from 'components/Elements/Forms';
import { SubmitHandler, useForm } from 'react-hook-form';
import { useRouter } from 'next/router';
import { SuccessToast, FailToast } from 'components/Toast';
import axios from 'axios';
import ROUTE from 'config/api/route';
import { Dispatch } from 'react';

type FormValues = {
  judul: string;
  deskripsi: string;
  gaji_min: number;
  gaji_max: number;
  skillset: string[];
  jenis: string;
};

const SKILLS = [
  'Product Design',
  'System Engineer',
  'BackEnd Engineer',
  'FrontEnd Engineer',
  'Mobile Developer',
];

const JOB_TYPE = [
  { title: 'Full-time', value: 'full_time' },
  { title: 'Part-time', value: 'part_time' },
  { title: 'Internship', value: 'internship' },
];

export const UpdateJobPostModal = ({
  jobPostData,
  loggedIn,
  setJobPostData,
}: {
  jobPostData: any;
  loggedIn: any;
  setJobPostData: Dispatch<any>;
}) => {
  const payload: any = {};
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();
  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<FormValues>();

  const onSubmit: SubmitHandler<FormValues> = (data) => {
    return new Promise<void>(async (resolve, reject) => {
      try {
        if (loggedIn) {
          const req = await axios.put(ROUTE.JOB(jobPostData.id), data);
          const res = req.data;
          setJobPostData(res);
          onClose();
          SuccessToast({
            title: 'Lowongan Pekerjaan Berhasil Diupdate 🤩',
            desc: '',
          });
          setTimeout(() => {
            resolve();
          }, 2000);
        } else {
          onClose();
          FailToast({
            title: 'Anda belum terautentikasi',
            desc: 'Silakan login untuk melamar pekerjaan',
          });
          setTimeout(() => {
            reject();
            router.push('/login');
          }, 2000);
        }
      } catch (error) {
        onClose();
        FailToast({
          title: 'Lowongan Pekerjaan Gagal Diupdate 😢',
          desc: 'Silakan coba lagi',
        });
        setTimeout(() => {
          resolve();
        }, 2000);
      }
    });
  };
  return (
    <>
      <Button ml={3} alignSelf="center" onClick={onOpen} variant="solid" colorScheme="green">
        Update
      </Button>
      <Modal
        isOpen={isOpen}
        onClose={onClose}
        isCentered
        closeOnOverlayClick={false}
        scrollBehavior={'inside'}
        size={'xl'}
      >
        <ModalOverlay />
        <ModalContent
          display="flex"
          boxShadow="0px 0px 50px rgba(0, 0, 0, 0.25)"
          borderRadius="24px"
          padding={{ lg: '48px', base: '24px' }}
          justifyContent="center"
          alignItems="center"
        >
          <ModalHeader
            fontSize={{ base: '18px', lg: '24px' }}
            color="siputTeal.500"
            fontWeight="700"
          >
            Update Lowongan Pekerjaan
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody width="100%">
            <form onSubmit={handleSubmit(onSubmit)}>
              <VStack spacing="20px" mt="30px">
                <TextInput
                  id="judul"
                  defaultValue={jobPostData.judul}
                  register={register}
                  title="Judul Lowongan Pekerjaan"
                  errors={errors}
                  rules={{
                    required: 'Ini Wajib Di isi',
                    minLength: { value: 4, message: 'Minimal panjang pada judul adalah 4' },
                  }}
                />

                <RadioButtonInput
                  colorScheme="teal"
                  title="Tipe Pekerjaan"
                  id="types"
                  defaultValue={jobPostData.jenis}
                  register={register}
                  errors={errors}
                  options={JOB_TYPE}
                />

                <TextAreaInput
                  id="deskripsi"
                  register={register}
                  defaultValue={jobPostData.deskripsi}
                  title="Deskripsi Lowongan Pekerjaan"
                  errors={errors}
                  rules={{
                    required: 'Ini Wajib Di isi',
                    minLength: { value: 10, message: 'Minimal panjang pada deskripsi adalah 10' },
                  }}
                />

                <NumberInputs
                  id="gaji_min"
                  register={register}
                  defaultValue={jobPostData.gaji_min}
                  title="Gaji Minimum"
                  errors={errors}
                />

                <NumberInputs
                  id="gaji_max"
                  register={register}
                  defaultValue={jobPostData.gaji_max}
                  title="Gaji Maksimum"
                  errors={errors}
                />

                <CheckboxInput
                  colorScheme="teal"
                  defaultValue={jobPostData.skillset}
                  title="Jenis Pekerjaan"
                  id="skillset"
                  register={register}
                  errors={errors}
                  options={SKILLS}
                />
                <Flex flexDir={'row'}>
                  <Button onClick={onClose} mr="20px" variant="outline" colorScheme="green">
                    Cancel
                  </Button>
                  <Button
                    variant={'solid'}
                    colorScheme="green"
                    type="submit"
                    isLoading={isSubmitting}
                  >
                    Update
                  </Button>
                </Flex>
              </VStack>
            </form>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
};
