import { Text } from '@chakra-ui/layout';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
} from '@chakra-ui/react';
import { useDisclosure } from '@chakra-ui/hooks';
import { FaTrashAlt } from 'react-icons/fa';
import { Button } from '@chakra-ui/button';
import { useRouter } from 'next/router';
import { SuccessToast, FailToast } from 'components/Toast';
import axios from 'axios';
import ROUTE from 'config/api/route';

export const DeleteJobPostModal = ({
  jobPostData,
  loggedIn,
}: {
  jobPostData: any;
  loggedIn: any;
}) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter();

  function onDelete() {
    return new Promise<void>(async (resolve, reject) => {
      try {
        if (loggedIn) {
          await axios.delete(ROUTE.JOB(jobPostData.id));
          onClose();
          SuccessToast({
            title: 'Lowongan Pekerjaan Berhasil Dihapus 🥺',
            desc: '',
          });
          setTimeout(() => {
            resolve();
            router.push('/perusahaan');
          }, 1500);
        } else {
          onClose();
          FailToast({
            title: 'Anda belum terautentikasi',
            desc: 'Silakan login untuk melamar pekerjaan',
          });
          setTimeout(() => {
            reject();
            router.push('/login');
          }, 2000);
        }
      } catch (error) {
        onClose();
        FailToast({
          title: 'Lowongan Pekerjaan Gagal Dihapu 😢',
          desc: 'Silakan coba lagi',
        });
        setTimeout(() => {
          resolve();
          router.reload();
        }, 2000);
      }
    });
  }

  return (
    <>
      <Button
        mr={3}
        alignSelf="center"
        onClick={onOpen}
        variant="solid"
        colorScheme="red"
        mx="24px"
      >
        Hapus
      </Button>
      <Modal isOpen={isOpen} onClose={onClose} isCentered closeOnOverlayClick={false}>
        <ModalOverlay />
        <ModalContent
          display="flex"
          boxShadow="0px 0px 50px rgba(0, 0, 0, 0.25)"
          borderRadius="24px"
          padding={{ lg: '48px', base: '24px' }}
          justifyContent="center"
          alignItems="center"
        >
          <FaTrashAlt color="red" size={64} />
          <ModalHeader fontSize={{ base: '18px', lg: '24px' }} fontWeight="700">
            Hapus Lowongan Pekerjaan
          </ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <Text
              fontSize={{ base: '14px', lg: '16px' }}
              color="#4F4F4F"
              fontWeight="400"
              textAlign="center"
            >
              Dengan menghapus lowongan pekerjaan ini, kamu akan kehilangan semua data dan lowongan
              pekerjaan tidak akan ditampilkan.
            </Text>
          </ModalBody>

          <ModalFooter>
            <Button mr={3} onClick={onClose} variant="outline" colorScheme="red">
              Batal
            </Button>

            <Button onClick={onDelete} variant="solid" colorScheme="red">
              Hapus
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};
