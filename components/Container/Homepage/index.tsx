import { Button } from '@chakra-ui/button';
import { Box, Flex, Text } from '@chakra-ui/layout';
import { PX } from 'components/Layout/Navbar';
import { FC } from 'react';
import HomeTitle from './HomeTitle';
import Link from 'next/link';
import { useSelector } from 'react-redux';
import { isLoggedin } from 'config/store/auth/selector';
import { RootState } from 'config/store/reducers';

const Homepage: FC = () => {
  const isAuth = useSelector(isLoggedin);
  const userRole = useSelector((state: RootState) => state.auth.user?.user_role);
  const isEmployer = userRole === 'employer';
  const isAdmin = userRole === 'admin';
  return (
    <Box
      h="full"
      minH="100vh"
      mt={{ base: '-75px', md: '-100px' }}
      pt={100}
      px={PX}
      sx={{
        background: 'linear-gradient(99.28deg, #E6FFFA 0.32%, rgba(255, 255, 255, 0) 99.12%)',
      }}
    >
      <Flex justify="space-between" align="center" py={5} h="full">
        <Box flex={1}>
          <HomeTitle />
          <Box color="#193E6C" fontWeight={500}>
            Temukan perjalanan karir Anda berikutnya dengan lebih dari <Text as="b">15.000 </Text>
            lowongan kerja diantaranya customer support, software engineer, dan designer di mana
            saja di dunia ini baik secara remote maupun onsite.
          </Box>
          <Flex gridGap={5} mt={10}>
            {!isAdmin && !isEmployer ? (
              <Link href={isAuth ? '/profile' : '/login'}>
                <a>
                  <Button fontWeight="normal" variant="outline" colorScheme="teal">
                    <Box as="span" fontWeight="semibold" mr={1}>
                      {isAuth ? 'Lihat Profile' : 'Login Dulu'}
                    </Box>
                    🐱‍💻
                  </Button>
                </a>
              </Link>
            ) : (
              ''
            )}
            <Link href="/lowongan-kerja">
              <a>
                <Button fontWeight="normal" colorScheme="teal">
                  <Box as="span" fontWeight="semibold" mr={1}>
                    Cari Sekarang
                  </Box>
                  🧐
                </Button>
              </a>
            </Link>
          </Flex>
        </Box>
        <Box flex={1} display={{ base: 'none', md: 'block' }}>
          <Box
            as="img"
            ml="auto"
            width="full"
            maxW="450px"
            src="https://cdn.statically.io/gl/MabaKating/cdn/main/home/hero.png"
            alt="hero"
          />
        </Box>
      </Flex>
    </Box>
  );
};

export default Homepage;
