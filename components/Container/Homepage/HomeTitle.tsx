import { Box } from '@chakra-ui/layout';

const HomeTitle = () => {
  return (
    <Box fontSize="5xl" py={6}>
      <Box as="span" fontWeight={900} color="#193E6C">
        Temukan Pekerjaan Impian Anda di Sini
      </Box>
      <span> 😍</span>
    </Box>
  );
};

export default HomeTitle;
