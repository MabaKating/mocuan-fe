import {
  Box,
  Flex,
  Text,
  Button,
  Badge,
  List,
  ListItem,
  ListIcon,
  Spacer,
  Spinner,
  Center,
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { MdCheckCircle, MdArrowBack } from 'react-icons/md';
import { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { SuccessToast, FailToast, WarningToast } from 'components/Toast';
import { UpdateJobPostModal, DeleteJobPostModal } from 'components/Modals';
import axios from 'axios';
import ROUTE from 'config/api/route';
import { useSelector } from 'react-redux';
import Markdown from 'react-markdown';
import { isLoggedin } from 'config/store/auth/selector';
import { Link } from '@chakra-ui/react';

const JobPostDetail = ({ jobPostData: Job }: { jobPostData: any }) => {
  const [applied, setApplied] = useState(false);
  const [date, setDate] = useState('');
  const loggedIn = useSelector(isLoggedin);
  const [userRole, setUserRole] = useState('');
  const [employerId, setEmployerId] = useState(0);
  const [isLoaded, setIsLoaded] = useState(false);
  const [jobPostData, setJobPostData] = useState(Job);

  function employerPermissions() {
    if (userRole === 'employer' && loggedIn && employerId === jobPostData?.company?.employer) {
      return true;
    }
    return false;
  }

  useEffect(() => {
    setJobPostData(Job);
  }, [Job]);

  const fetchApplication = async () => {
    return new Promise<Application[]>(async (resolve, reject) => {
      try {
        const currUser = await axios.get(ROUTE.CURRENT_USER);
        setUserRole(currUser.data.user_role);
        setEmployerId(currUser.data.employer);
        const jobSeeker = await axios.get(ROUTE.JOBSEEKER(currUser.data.jobseeker));
        const appl = jobSeeker.data.applications.filter(
          (appl: Application) => appl.job === jobPostData.id,
        );
        setIsLoaded(true);
        resolve(appl);
      } catch (error) {
        reject(error);
      }
    });
  };

  const filterApplication = async () => {
    try {
      const appl = await fetchApplication();
      if (appl.length > 0) {
        setApplied(true);
      }
    } catch (error) {}
  };

  const getDate = () => {
    var today = new Date();
    var date = today.getDate() + '-' + (today.getMonth() + 1) + '-' + today.getFullYear();
    setDate(date);
  };

  useEffect(() => {
    getDate();
    if (jobPostData && loggedIn) {
      filterApplication();
    }
  }, [jobPostData]);

  const {
    handleSubmit,
    formState: { isSubmitting },
  } = useForm();
  const router = useRouter();

  function onApply() {
    return new Promise<void>(async (resolve, reject) => {
      if (loggedIn) {
        await axios.post(ROUTE.APPLICATIONS, {
          jobseeker: await axios.get(ROUTE.CURRENT_USER).then((res) => res.data.jobseeker),
          job: jobPostData?.id,
          date: date,
        });
      } else {
        FailToast({
          title: 'Anda belum terautentikasi',
          desc: 'Silakan login untuk melamar pekerjaan',
        });
        reject();
      }

      setTimeout(() => {
        resolve();
      }, 500);
    })
      .then(() => {
        setApplied(true);
        SuccessToast({
          title: 'Lamaran Anda berhasil dikirim',
          desc: 'Lamaran Anda akan segera kami proses',
        });
      })
      .catch(() => {
        if (loggedIn) {
          FailToast({ title: 'Lamaran Anda gagal dikirim', desc: 'Silakan coba lagi' });
        }
      });
  }

  function onCancel() {
    return new Promise<void>(async (resolve) => {
      const appl = await fetchApplication();
      await axios.delete(ROUTE.APPLICATION(appl[0].id));

      setTimeout(() => {
        resolve();
      }, 500);
    })
      .then(() => {
        setApplied(false);
        WarningToast({
          title: 'Lamaran Anda berhasil dibatalkan',
          desc: 'Lamaran Anda telah dibatalkan',
        });
      })
      .catch(() => {
        FailToast({ title: 'Lamaran Anda gagal dibatalkan', desc: 'Silakan coba lagi' });
      });
  }

  if (!jobPostData && isLoaded === false) {
    return (
      <Center>
        <Spinner
          mt={2}
          mx="auto"
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="#7928CA"
          size="xl"
        />
      </Center>
    );
  }

  return (
    <Box
      backgroundColor="white"
      boxShadow="rgba(0, 0, 0, 0.05) 0px 0px 32px 13px"
      maxW="xl"
      p={8}
      m="auto"
      borderRadius={8}
      mb={16}
    >
      <Box alignItems="center">
        <Text fontSize="2xl" fontWeight="bold">
          {jobPostData?.judul}
        </Text>
        <Text fontSize="lg" fontWeight="bold" mb={4}>
          <Link href={`/perusahaan/${jobPostData?.company?.id}`}>{jobPostData?.company?.nama}</Link>
        </Text>
        <Badge borderRadius="full" px={2} colorScheme="teal" mb={4}>
          {jobPostData?.jenis.replace('_', ' ')}
        </Badge>
        <Text fontSize="sm" fontWeight="bold">
          DESKRIPSI
        </Text>
        <Box fontSize="sm" mt={2}>
          {/* eslint-disable-next-line react/no-children-prop */}
          <Markdown children={jobPostData?.deskripsi} />
        </Box>
        <Text fontSize="sm" fontWeight="bold" mt={2}>
          GAJI
        </Text>
        <Text fontSize="sm">
          Rp{jobPostData?.gaji_min} - Rp{jobPostData?.gaji_max}
        </Text>
        <Text fontSize="sm" fontWeight="bold" mt={2}>
          SKILLS
        </Text>
        <List ml={'-1em'}>
          {jobPostData?.skillset.map((skill: any) => (
            <ListItem key={skill}>
              <Flex alignItems="center">
                <ListIcon as={MdCheckCircle} color="green.500" />
                <Text fontSize="sm">{skill}</Text>
              </Flex>
            </ListItem>
          ))}
        </List>
      </Box>

      <Box mt={8}>
        <Flex>
          <Button
            leftIcon={<MdArrowBack />}
            fontWeight="normal"
            variant="outline"
            colorScheme="teal"
            onClick={() => router.back()}
          >
            <Box as="span" fontWeight="semibold" mr={1}>
              Kembali
            </Box>
          </Button>

          <Spacer />

          {employerPermissions() ? (
            <Flex flexDir="row">
              <DeleteJobPostModal loggedIn={loggedIn} jobPostData={jobPostData} />
              <UpdateJobPostModal
                loggedIn={loggedIn}
                jobPostData={jobPostData}
                setJobPostData={setJobPostData}
              />
            </Flex>
          ) : applied ? (
            <Button
              fontWeight="normal"
              colorScheme="gray"
              isLoading={isSubmitting}
              type="submit"
              onClick={handleSubmit(onCancel)}
            >
              <Box as="span" fontWeight="semibold" mr={1}>
                Batalkan Lamaran 💔
              </Box>
            </Button>
          ) : (
            userRole !== 'employer' &&
            userRole !== 'admin' && (
              <Button
                fontWeight="normal"
                colorScheme="teal"
                isLoading={isSubmitting}
                type="submit"
                onClick={handleSubmit(onApply)}
              >
                <Box as="span" fontWeight="semibold" mr={1}>
                  Lamar Sekarang ❤️
                </Box>
              </Button>
            )
          )}
        </Flex>
      </Box>
    </Box>
  );
};

export default JobPostDetail;
