import { Box } from '@chakra-ui/layout';
import { Tab, TabList, TabPanel, TabPanels, Tabs } from '@chakra-ui/tabs';
import RejectedJobs from './RejectedJobs';
import UncheckJobs from './UncheckJobs';
import VerifiedJobs from './VerifiedJobs';

const Dashboard = () => {
  return (
    <Box w="full" maxW="900" mx="auto" pt={10}>
      <Tabs isLazy colorScheme="teal" align="center">
        <TabList>
          <Tab>👀 Belum Diperiksa</Tab>
          <Tab>✅ Diterima</Tab>
          <Tab>❌ Ditolak</Tab>
        </TabList>
        <TabPanels>
          <TabPanel>
            <UncheckJobs />
          </TabPanel>
          <TabPanel>
            <VerifiedJobs />
          </TabPanel>
          <TabPanel>
            <RejectedJobs />
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Box>
  );
};

export default Dashboard;
