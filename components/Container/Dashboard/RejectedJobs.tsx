import { Button } from '@chakra-ui/button';
import { Box, Flex, StackDivider, VStack } from '@chakra-ui/layout';
import axios from 'axios';
import ROUTE from 'config/api/route';
import { useEffect, useState } from 'react';
import Link from 'next/link';
import { Spinner } from '@chakra-ui/spinner';

const RejectedJobs = () => {
  const [jobs, setJobs] = useState<Job[]>([]);
  const [isLoading, setLoading] = useState(true);

  const fetchJobs = async () => {
    try {
      const req = await axios.get(ROUTE.JOBS, { params: { status: 'rejected' } });
      const res = req.data;
      setJobs(res);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchJobs();
  }, []);
  return (
    <VStack divider={<StackDivider borderColor="gray.200" />} spacing={3} align="stretch">
      {isLoading ? (
        <Spinner
          mt={2}
          mx="auto"
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="#7928CA"
          size="xl"
        />
      ) : (
        jobs.map((item, index) => (
          <Flex key={index} justify="space-between" align="center" w="full">
            <Box textAlign="left">
              <b>{item.judul}</b>
              <p>{item.company?.nama}</p>
            </Box>
            <Link href={`/lowongan-kerja/${item.id}`}>
              <a>
                <Button w="full" colorScheme="blue">
                  Detail
                </Button>
              </a>
            </Link>
          </Flex>
        ))
      )}
    </VStack>
  );
};

export default RejectedJobs;
