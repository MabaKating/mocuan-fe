import { Button, ButtonGroup } from '@chakra-ui/button';
import { Box, Flex, StackDivider, VStack } from '@chakra-ui/layout';
import { Spinner } from '@chakra-ui/spinner';
import axios from 'axios';
import ROUTE from 'config/api/route';
import { useEffect, useState } from 'react';
import Link from 'next/link';

const UncheckJobs = () => {
  const [jobs, setJobs] = useState<Job[]>([]);
  const [isLoading, setLoading] = useState(true);

  const fetchJobs = async () => {
    try {
      const req = await axios.get(ROUTE.JOBS, { params: { status: 'uncheck' } });
      const res = req.data;
      setJobs(res);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchJobs();
  }, []);

  const PerformAccept = async (id: string) => {
    try {
      const req = await axios.put(ROUTE.JOB(id), { status: 'verified' });
      const res = await req.data;

      const filtered = jobs.filter((item) => item.id !== res.id);
      setJobs(filtered);
    } catch (error) {}
  };
  const PerformReject = async (id: string) => {
    try {
      const req = await axios.put(ROUTE.JOB(id), { status: 'rejected' });
      const res = await req.data;

      const filtered = jobs.filter((item) => item.id !== res.id);
      setJobs(filtered);
    } catch (error) {}
  };
  return (
    <VStack divider={<StackDivider borderColor="gray.200" />} spacing={3} align="stretch">
      {isLoading ? (
        <Spinner
          mt={2}
          mx="auto"
          thickness="4px"
          speed="0.65s"
          emptyColor="gray.200"
          color="#7928CA"
          size="xl"
        />
      ) : (
        jobs.map((item, index) => (
          <Flex key={index} justify="space-between" align="center" w="full">
            <Link href={`/lowongan-kerja/${item.id}`} passHref>
              <Box as="a" textAlign="left">
                <b>{item.judul}</b>
                <p>{item.company?.nama}</p>
              </Box>
            </Link>
            <ButtonGroup spacing="2">
              <Button onClick={() => PerformReject(item.id)} colorScheme="red" variant="outline">
                Tolak
              </Button>
              <Button onClick={() => PerformAccept(item.id)} colorScheme="green">
                Terima
              </Button>
            </ButtonGroup>
          </Flex>
        ))
      )}
    </VStack>
  );
};

export default UncheckJobs;
