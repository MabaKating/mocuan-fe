// @ts-nocheck

import { Flex, Heading } from '@chakra-ui/layout';
import { Image } from '@chakra-ui/image';
import FormBuat from './Form/FormBuat';

export default function PageBuatPerusahaan({ setCompany }) {
  return (
    <Flex
      padding="0 15% 7%"
      margin="0 auto"
      maxWidth="110em"
      height="60em"
      justifyContent="center"
      flexDirection="column"
      textAlign="center"
      alignItems="center"
    >
      <Image
        objectFit="contain"
        src="/company/no-company.svg"
        alt="No Company"
        maxWidth="55em"
        mb={8}
      />
      <Heading as="h6" size="lg" fontWeight="700">
        Belum terdapat profil perusahaan
      </Heading>
      <FormBuat setCompany={setCompany} />
    </Flex>
  );
}
