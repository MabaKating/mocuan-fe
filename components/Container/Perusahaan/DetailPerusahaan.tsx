// @ts-nocheck
import { Box, Stack, Text, Heading, HStack, VStack, Flex } from '@chakra-ui/layout';
import { Button } from '@chakra-ui/react';
import { Image } from '@chakra-ui/image';
import { FaMapMarkerAlt, FaBuilding, FaUsers, FaGlobe } from 'react-icons/fa';
import FormUpdate from './Form/FormUpdate';
import FormHapus from './Form/FormHapus';
import Link from 'next/link';
import config from 'config/api/domain';
import JobPostCard from 'components/Card/JobPostCard';

export default function DetailPerusahaan({ company, user, setCompany, jobs }) {
  const getJobsData = () => {
    return company.jobs == undefined ? jobs : company.jobs;
  };

  return (
    <VStack padding="0 12% 7%" margin="0 auto" maxWidth="110em" justifyContent="center">
      {user?.user_role == 'employer' &&
        (user?.employer.id == company?.employer || user?.employer.id == company?.employer.id) && (
          <Box>
            <Stack direction="row" spacing={4} mb="0.5rem">
              <FormUpdate company={company} setCompany={setCompany} />
              <FormHapus companyId={company.id} />
            </Stack>
          </Box>
        )}
      <Box>
        <Image
          alt="Banner Perusahaan"
          src={config.API_BASE_URL + company.banner?.url}
          fallbackSrc="https://via.placeholder.com/1120x280?text=."
        />
      </Box>
      <HStack
        pt={{ xs: '1rem', md: '1em', lg: '3em' }}
        w="100%"
        justifyContent="center"
        flexWrap="wrap"
        spacing={{ sm: '58px' }}
      >
        <Box mb={{ xs: '1rem', md: '0.5rem' }}>
          <Image
            boxSize={{ xs: '50px', xl: '130px' }}
            fit="contain"
            src={config.API_BASE_URL + company.logo?.url}
            alt="Logo Perusahaan"
            fallbackSrc="https://via.placeholder.com/130?text=."
          />
        </Box>
        <Stack maxWidth="760px" lineHeight="30px">
          <HStack paddingBottom="1.5rem" pl="0">
            <Heading as="h4">{company.nama}</Heading>
          </HStack>
          <HStack spacing="15px">
            <FaMapMarkerAlt />
            <Text textAlign="justify">{company.lokasi}</Text>
          </HStack>
          <HStack spacing="15px">
            <FaBuilding />
            <Text>{company.jenisIndustri}</Text>
          </HStack>
          <HStack spacing="15px">
            <FaUsers />
            <Text>{company.jumlahPekerja} karyawan</Text>
          </HStack>
          {company.website !== '' && (
            <HStack spacing="15px">
              <FaGlobe />
              <Text>{company.website}</Text>
            </HStack>
          )}
        </Stack>
      </HStack>

      <Stack padding="8% 1%" maxWidth="920px" lineHeight="30px">
        <Heading as="h5" fontSize="24px" pb="1em" fontWeight="600">
          Tentang Perusahaan
        </Heading>
        <Text textAlign="justify">{company.deskripsi}</Text>
      </Stack>

      <Stack padding="0 1%" maxWidth="920px">
        <Flex flexDir="row" justifyContent="space-between">
          <Heading as="h5" fontSize="24px" pb="2em" fontWeight="600">
            Lowongan pekerjaan yang dibuka
          </Heading>
          {user?.user_role == 'employer' && (
            <Link href="/lowongan-kerja/create">
              <a>
                <Button variant="solid" colorScheme={'green'}>
                  Buat Lowongan Pekerjaan Baru
                </Button>
              </a>
            </Link>
          )}
        </Flex>

        <Box width={{ xs: '95%', sm: '510px', md: '910px' }}>
          {getJobsData().map((job) => (
            <JobPostCard
              key={job.id}
              job={job}
              isMaintainable={false}
              href={`/lowongan-kerja/${job.id}`}
            />
          ))}
        </Box>
      </Stack>
    </VStack>
  );
}
