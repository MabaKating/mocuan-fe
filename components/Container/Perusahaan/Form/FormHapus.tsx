// @ts-nocheck
import { useForm } from 'react-hook-form';
import React from 'react';
import { Button } from '@chakra-ui/react';
import { useDisclosure } from '@chakra-ui/react';
import {
  Modal,
  ModalOverlay,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalFooter,
} from '@chakra-ui/modal';
import { Text } from '@chakra-ui/layout';
import Router from 'next/router';
import axios from 'axios';
import ROUTE from 'config/api/route';
import { SuccessToast } from 'components/Toast';

export default function FormHapus({ companyId }) {
  const { isOpen, onOpen, onClose } = useDisclosure();

  const initialRef = React.useRef();
  const finalRef = React.useRef();

  const {
    formState: { isSubmitting },
  } = useForm();

  function hapusCompany() {
    axios.delete(ROUTE.COMPANY(companyId)).then(() => {
      Router.push('/');
      SuccessToast({
        title: 'Berhasil Menghapus Profil Perusahaan',
        desc: '',
      });
    });
  }

  return (
    <>
      <Button onClick={onOpen} width="6rem" colorScheme="red" variant="outline">
        Hapus
      </Button>
      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
        size="2xl"
      >
        <ModalOverlay />
        <ModalContent p={2}>
          <ModalHeader>Hapus Profil Perusahaan</ModalHeader>
          <ModalCloseButton mt={4} mr={4} />

          <ModalBody pt={6}>
            <Text>Apakah Anda yakin ingin menghapus Profil Perusahaan?</Text>
          </ModalBody>

          <ModalFooter mt={10} mr={4}>
            <Button colorScheme="#EDF2F7" mr={2} textColor="teal" width="8em" onClick={onClose}>
              Batal
            </Button>
            <Button
              colorScheme="red"
              onClick={hapusCompany}
              isLoading={isSubmitting}
              type="submit"
              width="8em"
            >
              Hapus
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
