// @ts-nocheck
import { useForm } from 'react-hook-form';
import React from 'react';
import {
  FormErrorMessage,
  FormLabel,
  FormControl,
  Button,
  Textarea,
  Select,
} from '@chakra-ui/react';
import { Flex } from '@chakra-ui/layout';
import { useDisclosure } from '@chakra-ui/react';
import {
  Modal,
  ModalOverlay,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalFooter,
} from '@chakra-ui/modal';
import FileUpload from 'components/Elements/Forms/FileUpload';
import industrySectors from 'public/company/industry-sectors.json';
import tipeKaryawan from 'public/company/tipe-karyawan.json';
import { TextInput } from 'components/Elements/Forms';
import Asterix from 'components/Elements/Forms/Asterix';
import config from 'config/api/domain';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { RootState } from 'config/store/reducers';
import ROUTE from 'config/api/route';

export default function FormBuat({ setCompany }) {
  const auth = useSelector((state: RootState) => state.auth);
  const rootstate = useSelector((state: RootState) => state);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const initialRef = React.useRef() as React.MutableRefObject<HTMLInputElement>;
  const finalRef = React.useRef() as React.MutableRefObject<HTMLInputElement>;

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm();

  const uploadImage = async (data) => {
    const formData = new FormData();
    const formBanner = new FormData();
    var bannerId;
    formData.append('files', data.file_[0]);
    if (data.banner_[0] != null) {
      formBanner.append('files', data.banner_[0]);
      await axios.post(`${config.API_BASE_URL}/upload`, formBanner).then((response) => {
        bannerId = response.data[0].id;
      });
    }
    axios
      .post(`${config.API_BASE_URL}/upload`, formData)
      .then((response) => {
        const imageId = response.data[0].id;
        const dataSubmit = {};
        dataSubmit['nama'] = data.nama;
        dataSubmit['lokasi'] = data.lokasi;
        dataSubmit['jenisIndustri'] = data.jenisIndustri;
        dataSubmit['jumlahPekerja'] = data.jumlahPekerja;
        dataSubmit['deskripsi'] = data.deskripsi;

        if (data.website != undefined) {
          dataSubmit['website'] = data.website;
        }
        dataSubmit['logo'] = imageId;
        if (data.banner_[0] != undefined) {
          dataSubmit['banner'] = bannerId;
        }
        axios
          .post(ROUTE.COMPANIES, dataSubmit)
          .then((response) => {
            const thisCompanyId = response.data.id;
            axios
              .put(ROUTE.COMPANY(thisCompanyId), { employer: auth.user?.employer.id })
              .then((response) => {
                setCompany(response.data);
              });
          })
          .catch((error) => {});
      })
      .catch((error) => {});
  };

  const validateFiles = (value: any) => {
    if (value === undefined || value.length < 1) {
      return 'Mohon masukkan file gambar';
    }

    for (const file of Array.from(value)) {
      const fsMb = file.size / (1024 * 1024);
      const MAX_FILE_SIZE = 10;
      if (fsMb > MAX_FILE_SIZE) {
        return 'Max file size 10mb';
      }
    }
    return true;
  };

  return (
    <>
      <Button onClick={onOpen} colorScheme="teal" size="lg" py={5} px="3rem" my={8}>
        Buat Profil Perusahaan
      </Button>
      <Modal
        initialFocusRef={initialRef}
        finalFocusRef={finalRef}
        isOpen={isOpen}
        onClose={onClose}
        size="4xl"
        colorScheme="red"
      >
        <ModalOverlay />
        <ModalContent p={2}>
          <ModalHeader>Buat Profil Perusahaan</ModalHeader>
          <ModalCloseButton mt={4} mr={4} />

          <ModalBody mx={8} mt={4}>
            <form onSubmit={handleSubmit(uploadImage)}>
              <TextInput
                id="nama"
                title="Nama Perusahaan"
                register={register}
                placeholder="Nama Perusahaan"
                errors={errors}
                rules={{
                  required: 'Wajib diisi',
                  minLength: { value: 3, message: 'Minimum length should be 3' },
                }}
                mb={5}
              />

              <FormControl isInvalid={errors.lokasi} mb={5}>
                <FormLabel htmlFor="lokasi">
                  Lokasi Perusahaan <Asterix isRequired={true} />
                </FormLabel>
                <Textarea
                  id="lokasi"
                  placeholder="Lokasi Perusahaan"
                  {...register('lokasi', {
                    required: 'Wajib diisi',
                    minLength: { value: 10, message: 'Minimum length should be 10' },
                  })}
                />
                <FormErrorMessage>{errors.lokasi && errors.lokasi.message}</FormErrorMessage>
              </FormControl>

              <Flex gridGap={{ sm: 6 }} flexWrap={{ xs: 'wrap', sm: 'nowrap' }}>
                <FormControl isInvalid={errors.jenisIndustri} mb={5}>
                  <FormLabel htmlFor="jenisIndustri">
                    Jenis Industri <Asterix isRequired={true} />
                  </FormLabel>
                  <Select
                    id="jenisIndustri"
                    placeholder="Pilih"
                    {...register('jenisIndustri', {
                      required: 'Wajib diisi',
                    })}
                  >
                    {industrySectors.map(({ key, label }) => (
                      <option key={key} value={label}>
                        {label}
                      </option>
                    ))}
                  </Select>
                  <FormErrorMessage>
                    {errors.jenisIndustri && errors.jenisIndustri.message}
                  </FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.jumlahPekerja} mb={5}>
                  <FormLabel htmlFor="jumlahPekerja">
                    Jumlah Pekerja <Asterix isRequired={true} />
                  </FormLabel>
                  <Select
                    id="jumlahPekerja"
                    placeholder="Pilih"
                    {...register('jumlahPekerja', {
                      required: 'Wajib diisi',
                    })}
                  >
                    {tipeKaryawan.map(({ key, value }) => (
                      <option key={key} value={value}>
                        {value}
                      </option>
                    ))}
                  </Select>
                  <FormErrorMessage>
                    {errors.jumlahPekerja && errors.jumlahPekerja.message}
                  </FormErrorMessage>
                </FormControl>
              </Flex>

              <TextInput
                id="website"
                title="Website"
                register={register}
                placeholder="Website"
                errors={errors}
                rules={{
                  required: false,
                }}
                mb={5}
              />

              <FormControl isInvalid={errors.deskripsi} mb={5}>
                <FormLabel htmlFor="deskripsi">
                  Deskripsi Perusahaan <Asterix isRequired={true} />
                </FormLabel>
                <Textarea
                  id="deskripsi"
                  placeholder="Deskripsi Perusahaan"
                  {...register('deskripsi', {
                    required: 'Wajib diisi',
                    minLength: { value: 10, message: 'Minimum length should be 10' },
                  })}
                />
                <FormErrorMessage>{errors.deskripsi && errors.deskripsi.message}</FormErrorMessage>
              </FormControl>

              <Flex gridGap={{ sm: 6 }} flexWrap={{ xs: 'wrap', sm: 'nowrap' }}>
                <FormControl isInvalid={!!errors.file_} mb={5}>
                  <FormLabel>
                    {'Logo Perusahaan'} <Asterix isRequired={true} />
                  </FormLabel>

                  <FileUpload
                    accept={'image/*'}
                    multiple={false}
                    register={register('file_', { validate: validateFiles })}
                  >
                    <Button>Upload Logo</Button>
                  </FileUpload>
                  <FormErrorMessage>{errors.file_ && errors?.file_.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={!!errors.banner_} mb={5}>
                  <FormLabel>{'Banner Perusahaan'}</FormLabel>

                  <FileUpload accept={'image/*'} multiple={false} register={register('banner_')}>
                    <Button>Upload Banner</Button>
                  </FileUpload>
                  <FormErrorMessage>{errors.banner_ && errors?.banner_.message}</FormErrorMessage>
                </FormControl>
              </Flex>

              <ModalFooter mt={8} mr={-6}>
                <Button colorScheme="white" textColor="red" mr={2} width="8em" onClick={onClose}>
                  Batal
                </Button>
                <Button colorScheme="teal" isLoading={isSubmitting} type="submit" width="8em">
                  Buat
                </Button>
              </ModalFooter>
            </form>
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
