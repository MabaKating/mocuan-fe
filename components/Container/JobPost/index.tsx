import { Button } from '@chakra-ui/button';
import { InputGroup } from '@chakra-ui/input';
import { Box, Flex, Text } from '@chakra-ui/layout';
import { BsBookmark } from 'react-icons/bs';
import FilterJobPostCard from 'components/Card/FilterJobPostCard';
import JobPostCard from 'components/Card/JobPostCard';
import { TextInput } from 'components/Elements/Forms';
import { SubmitHandler, useForm } from 'react-hook-form';
import { FC, useEffect, useState } from 'react';
import axios from 'axios';
import ROUTE from 'config/api/route';
import { Spinner } from '@chakra-ui/spinner';

export interface FilterValues {
  judul_contains?: string;
  jenis?: string[];
  skillset_contains?: string[];
  gaji_min_gte?: number;
  gaji_max_lte?: number;
}
const INITIAL_FILTER = {
  judul_contains: '',
  jenis: [],
  skillset_contains: [],
  gaji_min_gte: undefined,
  gaji_max_lte: undefined,
};

const Jobpost: FC = () => {
  const sourceForm = useForm<FilterValues>({ defaultValues: INITIAL_FILTER });
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = sourceForm;

  const [jobs, setJobs] = useState<Job[]>([]);
  const [isLoading, setLoading] = useState(true);

  const onSubmit: SubmitHandler<FilterValues> = (values) => {
    if (values.jenis) {
      values = {
        ...values,
        jenis: values.jenis.map((item) => item.toLowerCase().replaceAll('-', '_')),
      };
    }
    if (!values.gaji_max_lte) {
      delete values.gaji_max_lte;
    }
    if (!values.gaji_min_gte) {
      delete values.gaji_min_gte;
    }
    if (!values.judul_contains) {
      delete values.judul_contains;
    }
    fetchJobs(values);
  };

  const fetchJobs = async (params = {}) => {
    try {
      setLoading(true);
      const req = await axios.get(ROUTE.JOBS, { params: { status: 'verified', ...params } });
      const res = req.data;
      setJobs(res);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchJobs();
  }, []);

  return (
    <Flex
      padding={{ lg: '48px', base: '24px' }}
      justifyContent="center"
      alignItems="center"
      flexDir="column"
    >
      <Flex flexDir="row" width="80%" mb="24px" justifyContent="space-between">
        <Text fontSize="24px" fontWeight="700" color="green">
          List Lowongan Pekerjaan
        </Text>
        <Button leftIcon={<BsBookmark />} colorScheme="teal" variant="ghost">
          Lihat lowongan tersimpan
        </Button>
      </Flex>
      <Flex as="form" width="80%" onSubmit={handleSubmit(onSubmit)}>
        <InputGroup>
          <TextInput
            id="judul_contains"
            register={register}
            placeholder="Cari lowongan Pekerjaan"
            errors={errors}
            mt="-8px"
          />
        </InputGroup>
        <Button type="submit" colorScheme="green" ml="20px">
          Cari
        </Button>
      </Flex>

      <Flex width="80%" mt="56px">
        <Box width="25%">
          <FilterJobPostCard sourceForm={sourceForm} onSubmit={onSubmit} />
        </Box>
        <Box width="75%" paddingLeft="24px" textAlign="center">
          {isLoading ? (
            <Spinner
              mt={5}
              thickness="4px"
              speed="0.65s"
              emptyColor="gray.200"
              color="#7928CA"
              size="xl"
            />
          ) : !!jobs.length ? (
            jobs.map((item, index) => (
              <JobPostCard
                key={index}
                job={item}
                isMaintainable={false}
                href={`/lowongan-kerja/${item.id}`}
              />
            ))
          ) : (
            <Box fontWeight="bold" mt={10}>
              Lowongan yang Anda cari tidak ditemukan
            </Box>
          )}
        </Box>
      </Flex>
    </Flex>
  );
};

export default Jobpost;
