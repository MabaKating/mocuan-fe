import CreateJobPostCard from 'components/Card/CreateJobPostCard';
import { Text, Flex, Button, VStack } from '@chakra-ui/react';
import {
  TextInput,
  CheckboxInput,
  RadioButtonInput,
  NumberInputs,
  TextAreaInput,
} from 'components/Elements/Forms';
import { useForm, SubmitHandler } from 'react-hook-form';
import { useRouter } from 'next/router';
import { SuccessToast, FailToast } from 'components/Toast';
import { useState, useEffect } from 'react';
import axios from 'axios';
import ROUTE from 'config/api/route';
import { useSelector } from 'react-redux';
import { RootState } from 'config/store/reducers';
import { isLoggedin } from 'config/store/auth/selector';

type FormValues = {
  judul: string;
  deskripsi: string;
  gaji_min: number;
  gaji_max: number;
  skillset: string[];
  jenis: string;
};

const SKILLS = [
  'Product Design',
  'System Engineer',
  'BackEnd Engineer',
  'FrontEnd Engineer',
  'Mobile Developer',
];

const JOB_TYPE = [
  { title: 'Full-time', value: 'full_time' },
  { title: 'Part-time', value: 'part_time' },
  { title: 'Internship', value: 'internship' },
];

const CreateJobPost = () => {
  const loggedIn = useSelector(isLoggedin);
  const auth = useSelector((state: RootState) => state.auth);
  const [isLoaded, setIsLoaded] = useState(false);
  const [employerId, setEmployerId] = useState(0);
  const [companyId, setCompanyId] = useState(0);
  const sourceForm = useForm<FormValues>();
  const router = useRouter();
  const payload: any = {};

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = sourceForm;

  useEffect(() => {
    const getUserData = async () => {
      try {
        const req = await axios.get(ROUTE.CURRENT_USER);
        const res = req.data;
        setEmployerId(res.employer);
        const reqCompany = await axios.get(ROUTE.EMPLOYER(res.employer));
        const resCompany = reqCompany.data;
        setCompanyId(resCompany.company.id);
      } catch (error) {
      } finally {
        setIsLoaded(true);
        setTimeout(() => {}, 300);
      }
    };
    getUserData();
  }, []);

  const onSubmit: SubmitHandler<FormValues> = (data) => {
    payload['judul'] = data.judul;
    payload['deskripsi'] = data.deskripsi;
    payload['jenis'] = data.jenis;
    payload['skillset'] = data.skillset;
    payload['gaji_min'] = data.gaji_min;
    payload['gaji_max'] = data.gaji_max;
    if (employerId > 0) {
      payload['employer'] = employerId;
    }
    if (companyId > 0) {
      payload['company'] = companyId;
    }

    return new Promise<void>(async (resolve, reject) => {
      try {
        if (loggedIn) {
          const req = await axios.post(ROUTE.JOBS, payload);
          const res = req.data;
          SuccessToast({
            title: 'Lowongan Pekerjaan Berhasil Dibuat ✨',
            desc: '',
          });
          setTimeout(() => {
            resolve();
            router.push('/perusahaan');
          }, 2000);
        } else {
          FailToast({
            title: 'Anda belum terautentikasi',
            desc: 'Silakan login untuk melamar pekerjaan',
          });
          reject();
        }
      } catch (error) {
        FailToast({
          title: 'Lowongan Pekerjaan Gagal Dibuat 😢',
          desc: 'Silakan coba lagi',
        });
        setTimeout(() => {
          resolve();
          router.reload();
        }, 2000);
      }
    });
  };

  if (isLoaded) {
    return (
      <Flex flexDir={'column'} alignItems={'center'}>
        <CreateJobPostCard>
          <Button pos="absolute" onClick={() => router.back()} alignSelf={'flex-start'}>
            🔙
          </Button>
          <Text fontSize={'24px'} fontWeight={'600'}>
            Buat Lowongan Pekerjaan Baru
          </Text>
          <form onSubmit={handleSubmit(onSubmit)} style={{ width: '80%' }}>
            <VStack spacing="12px" mt="30px">
              <TextInput
                id="judul"
                register={register}
                placeholder="Masukkan judul lowongan pekerjaan"
                title="Judul Lowongan Pekerjaan"
                errors={errors}
                rules={{
                  required: 'Ini Wajib Di isi',
                  minLength: { value: 4, message: 'Minimal panjang pada judul adalah 4' },
                }}
              />

              <RadioButtonInput
                colorScheme="teal"
                title="Tipe Pekerjaan"
                id="jenis"
                register={register}
                errors={errors}
                options={JOB_TYPE}
              />

              <TextAreaInput
                id="deskripsi"
                register={register}
                placeholder="Masukkan deskripsi pekerjaan"
                title="Deskripsi Pekerjaan"
                errors={errors}
                rules={{
                  required: 'Ini Wajib Di isi',
                  minLength: { value: 10, message: 'Minimal panjang pada deskripsi adalah 10' },
                }}
              />

              <NumberInputs
                id="gaji_min"
                register={register}
                placeholder="Masukkan besaran gaji minimum"
                title="Gaji Minimum"
                errors={errors}
              />

              <NumberInputs
                id="gaji_max"
                register={register}
                placeholder="Masukkan besaran gaji maksimum"
                title="Gaji Maksimum"
                errors={errors}
              />

              <CheckboxInput
                colorScheme="teal"
                title="Skill Yang Dibutuhkan"
                id="skillset"
                register={register}
                errors={errors}
                options={SKILLS}
              />
            </VStack>
            <Flex flexDir={'row'} alignItems={'flex-end'} justifyContent={'flex-end'} width="100%">
              <Button
                mr="20px"
                variant="outline"
                colorScheme={'green'}
                onClick={() => router.push('/perusahaan')}
              >
                Batal
              </Button>
              <Button variant={'solid'} isLoading={isSubmitting} colorScheme="green" type="submit">
                Buat
              </Button>
            </Flex>
          </form>
        </CreateJobPostCard>
      </Flex>
    );
  } else {
    return (
      // TODO: Implement state loading
      <>Loading....</>
    );
  }
};
export default CreateJobPost;
