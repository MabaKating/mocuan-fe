import { createStandaloneToast } from '@chakra-ui/react';
import theme from 'config/theme';

const customToast = createStandaloneToast({ theme: theme });

export const SuccessToast = ({ title, desc }: { title: any; desc: any }) =>
  customToast({
    title: title,
    description: desc,
    status: 'success',
    duration: 3000,
    isClosable: true,
    position: 'top',
  });
export const FailToast = ({ title, desc }: { title: any; desc: any }) =>
  customToast({
    title: title,
    description: desc,
    status: 'error',
    duration: 3000,
    isClosable: true,
    position: 'top',
  });

export const WarningToast = ({ title, desc }: { title: any; desc: any }) =>
  customToast({
    title: title,
    description: desc,
    status: 'warning',
    duration: 3000,
    isClosable: true,
    position: 'top',
  });
