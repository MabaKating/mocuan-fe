import { Flex, Text, Box } from '@chakra-ui/layout';
import { Button } from '@chakra-ui/button';

import { FiFilter } from 'react-icons/fi';

import { InputGroup, InputLeftAddon } from '@chakra-ui/input';
import { NumberInputs, CheckboxInput } from 'components/Elements/Forms';
import { FC } from 'react';
import { SubmitHandler, UseFormReturn } from 'react-hook-form';
import { FilterValues } from 'components/Container/JobPost';

const SKILLS = [
  'Product Design',
  'System Engineer',
  'BackEnd Engineer',
  'FrontEnd Engineer',
  'Mobile Developer',
];
const JOB_TYPE = ['Full-time', 'Part-time', 'Internship'];

interface IFilter {
  sourceForm: UseFormReturn<FilterValues, object>;
  onSubmit: SubmitHandler<FilterValues>;
}

const FilterJobPostCard: FC<IFilter> = ({ sourceForm, onSubmit }) => {
  const {
    handleSubmit,
    register,
    watch,
    formState: { errors, isSubmitting },
  } = sourceForm;
  const values = watch();

  return (
    <Flex
      as="form"
      onSubmit={handleSubmit(onSubmit)}
      flexDir="column"
      padding={{ lg: '18px', base: '12px' }}
      boxShadow="rgba(0, 0, 0, 0.05) 0px 0px 32px 12px"
      backgroundColor="rgb(255, 255, 255)"
      borderRadius="20px"
    >
      <Flex flexDir="row" alignItems="center" justify="flex-start" mb="12px">
        <FiFilter size={22} />
        <Text ml="12px">Filter Pekerjaan</Text>
      </Flex>

      <Box as="b" flex="1" textAlign="left">
        Tipe Pekerjaan
      </Box>

      <Box pb={4}>
        <CheckboxInput
          colorScheme="teal"
          id="jenis"
          register={register}
          errors={errors}
          options={JOB_TYPE}
        />
      </Box>

      <Box as="b" flex="1" textAlign="left">
        Skills
      </Box>

      <Box pb={4}>
        <CheckboxInput
          colorScheme="teal"
          id="skillset_contains"
          register={register}
          errors={errors}
          options={SKILLS}
        />
      </Box>

      <Box as="b" flex="1" textAlign="left">
        Gaji
      </Box>

      <Box pb={4}>
        <Box>Minimal</Box>
        <InputGroup>
          <InputLeftAddon px={2}>Rp</InputLeftAddon>
          <NumberInputs
            min={0}
            id="gaji_min_gte"
            rules={{
              validate: (value: number) =>
                !!values.gaji_min_gte
                  ? !!(+value <= +values.gaji_min_gte) ||
                    'Harga minimum harus lebih kecil dari harga maksimum.'
                  : true,
            }}
            errors={errors}
            register={register}
            step={50000}
          />
        </InputGroup>
        <Box>Maksimal</Box>
        <InputGroup>
          <InputLeftAddon px={2}>Rp</InputLeftAddon>
          <NumberInputs
            rules={{
              validate: (value: number) =>
                !!values.gaji_max_lte
                  ? !!(+value >= +values.gaji_max_lte) ||
                    'Harga maksimum harus lebih kecil dari harga minimum.'
                  : true,
            }}
            min={0}
            id="gaji_max_lte"
            errors={errors}
            register={register}
            step={50000}
          />
        </InputGroup>
      </Box>

      <Button type="submit" mt="12px" colorScheme="teal" fontSize="14px" isLoading={isSubmitting}>
        Terapkan Filter
      </Button>
    </Flex>
  );
};

export default FilterJobPostCard;
