import { Flex, Text } from '@chakra-ui/layout';
import { FaSuitcase, FaMapMarkerAlt } from 'react-icons/fa';
import { BsBookmark } from 'react-icons/bs';
import Link from 'next/link';
import { FC } from 'react';

interface IJobCard {
  job: Job;
  isMaintainable: boolean;
  href: string;
}

const CONVERT_JENIS: { [key: string]: string } = {
  part_time: 'Part-time',
  full_time: 'Full-time',
  internship: 'Internship',
};

const JobPostCard: FC<IJobCard> = ({ job, isMaintainable, href }) => {
  return (
    <Link href={href}>
      <a>
        <Flex
          flexDir="row"
          padding={{ lg: '18px', base: '12px' }}
          boxShadow="rgba(0, 0, 0, 0.05) 0px 0px 32px 13px"
          backgroundColor="rgb(255, 255, 255)"
          borderRadius="20px"
          mb="20px"
          justifyContent="space-between"
        >
          <Flex flexDir="column" justifyContent="center" align="start">
            <Text>{job?.company?.nama}</Text>
            <Text fontSize="32px" fontWeight="700">
              {job?.judul}
            </Text>
            <Flex flexDir="row">
              <FaSuitcase size={22} />
              <Text mx="8px">{CONVERT_JENIS[job?.jenis]}</Text>
              <FaMapMarkerAlt size={22} />
              <Text mx="8px">{job?.company?.lokasi}</Text>
            </Flex>
          </Flex>
          {/* TODO isMaintainable set to be true if user role === employer */}
          {isMaintainable ? '' : <BsBookmark color="green" size={22} />}
        </Flex>
      </a>
    </Link>
  );
};
export default JobPostCard;
