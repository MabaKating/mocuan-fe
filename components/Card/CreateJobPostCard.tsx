import { Flex } from '@chakra-ui/react';
import { FC } from 'react';

const CreateJobPostCard: FC = ({ children, ...props }) => {
  return (
    <Flex
      flexDir="column"
      width="70%"
      alignItems="center"
      my="36px"
      padding={{ lg: '24px', base: '12px' }}
      boxShadow="rgba(0, 0, 0, 0.05) 0px 0px 32px 12px"
      backgroundColor="rgb(255, 255, 255)"
      borderRadius="20px"
      {...props}
    >
      {children}
    </Flex>
  );
};
export default CreateJobPostCard;
