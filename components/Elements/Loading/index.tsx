import { Box, Flex } from '@chakra-ui/layout';
import { Spinner } from '@chakra-ui/spinner';
import Logo from '../Logo';

const Loading = () => {
  return (
    <Flex justify="center" align="center" h="100vh" w="full" direction="column">
      <Logo />
      <Spinner
        mt={2}
        thickness="4px"
        speed="0.65s"
        emptyColor="gray.200"
        color="#7928CA"
        size="xl"
      />
      <Box
        fontSize="lg"
        fontWeight={700}
        mt={2}
        bgGradient="linear(to-l, #7928CA, #FF0080)"
        bgClip="text"
      >
        Mohon ditunggu...😇
      </Box>
    </Flex>
  );
};

export default Loading;
