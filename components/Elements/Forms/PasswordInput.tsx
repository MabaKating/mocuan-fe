import { Button } from '@chakra-ui/button';
import { FormControl, FormErrorMessage, FormLabel } from '@chakra-ui/form-control';
import Icon from '@chakra-ui/icon';
import { Input, InputGroup, InputRightElement } from '@chakra-ui/input';
import { BoxProps } from '@chakra-ui/layout';
import { FC, useState } from 'react';
import { BsFillEyeFill, BsFillEyeSlashFill } from 'react-icons/bs';
import Asterix from './Asterix';

export const PasswordInput: FC<InputProps & BoxProps> = (props) => {
  const { id, errors, rules, register, title, placeholder, ...rest } = props;

  const [show, setShow] = useState(false);
  const handleClick = () => setShow(!show);

  return (
    <FormControl {...rest} isInvalid={errors[id]}>
      <FormLabel htmlFor={id}>
        {title} <Asterix isRequired={rules?.['required']} />
      </FormLabel>
      <InputGroup size="md">
        <Input
          type={show ? 'text' : 'password'}
          id={id}
          placeholder={placeholder}
          {...register(id, rules)}
        />
        <InputRightElement mr={3}>
          <Button h="1.75rem" size="sm" onClick={handleClick}>
            <Icon h={4} w={4} as={show ? BsFillEyeSlashFill : BsFillEyeFill} />
          </Button>
        </InputRightElement>
      </InputGroup>
      <FormErrorMessage>{errors[id] && errors[id].message}</FormErrorMessage>
    </FormControl>
  );
};
