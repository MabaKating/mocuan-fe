import { FormControl, FormErrorMessage, FormLabel } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';
import { BoxProps } from '@chakra-ui/layout';
import { FC } from 'react';
import Asterix from './Asterix';

export const TextInput: FC<InputProps & BoxProps> = (props) => {
  const { id, errors, rules, register, title, placeholder, defaultValue, ...rest } = props;

  return (
    <FormControl {...rest} isInvalid={errors[id]}>
      <FormLabel htmlFor={id}>
        {title} <Asterix isRequired={rules?.['required']} />
      </FormLabel>
      <Input
        defaultValue={defaultValue}
        id={id}
        placeholder={placeholder}
        {...register(id, rules)}
      />
      <FormErrorMessage>{errors[id] && errors[id].message}</FormErrorMessage>
    </FormControl>
  );
};
