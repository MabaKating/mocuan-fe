interface InputProps {
  errors: {
    [x: string]: any;
  };
  id: string;
  rules?: any;
  title?: string;
  register: UseFormRegister;
  placeholder?: string;
  defaulValue?: any;
}
