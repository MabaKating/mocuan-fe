import { Radio, RadioGroup, CheckboxGroupProps } from '@chakra-ui/react';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { VStack } from '@chakra-ui/layout';
import { FC } from 'react';
import { useState } from 'react';

interface IRadioButton extends InputProps {
  options: { title: string; value: string }[];
}

export const RadioButtonInput: FC<IRadioButton & CheckboxGroupProps> = (props) => {
  const { id, errors, rules, register, title, options, defaultValue, ...rest } = props;
  const [value, setValue] = useState('full_time');

  return (
    <FormControl isInvalid={errors[id]}>
      <FormLabel htmlFor={id}>{title}</FormLabel>
      <RadioGroup {...rest} name={id} onChange={setValue} value={value}>
        <VStack align="start">
          {options.map((item, index) => (
            <Radio key={index} value={item.value} {...register(id, rules)}>
              {item.title}
            </Radio>
          ))}
        </VStack>
      </RadioGroup>
    </FormControl>
  );
};
