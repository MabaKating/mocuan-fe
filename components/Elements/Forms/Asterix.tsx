import { Box } from '@chakra-ui/layout';
import { FC } from 'react';

const Required: FC<{ isRequired: boolean }> = ({ isRequired }) => {
  return isRequired ? (
    <Box as="span" color="red.500">
      *
    </Box>
  ) : (
    <></>
  );
};

export default Required;
