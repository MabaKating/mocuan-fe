import { FormControl, FormErrorMessage } from '@chakra-ui/form-control';
import { Input } from '@chakra-ui/input';
import { BoxProps } from '@chakra-ui/layout';
import { FC } from 'react';

export const TextInputNoTelpon: FC<InputProps & BoxProps> = (props) => {
  const { id, errors, rules, register, placeholder, defaultValue, ...rest } = props;

  return (
    <FormControl {...rest} isInvalid={errors[id]}>
      <Input
        defaultValue={defaultValue}
        id={id}
        placeholder={placeholder}
        {...register(id, rules)}
      />
      <FormErrorMessage>{errors[id] && errors[id].message}</FormErrorMessage>
    </FormControl>
  );
};
