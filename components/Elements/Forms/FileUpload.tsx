import { useRef } from 'react';
import { InputGroup } from '@chakra-ui/react';
import { UseFormRegisterReturn } from 'react-hook-form';
import { FC } from 'react';

type FileUploadProps = {
  register: UseFormRegisterReturn;
  accept?: string;
  multiple?: boolean;
  onChange?: any;
};

const FileUpload: FC<FileUploadProps> = (props) => {
  const { register, accept, multiple, children, onChange } = props;
  const inputRef = useRef<HTMLInputElement | null>(null);
  const { ref, ...rest } = register as { ref: (instance: HTMLInputElement | null) => void };

  const handleClick = () => inputRef.current?.click();

  return (
    <InputGroup onClick={handleClick}>
      <input
        type={'file'}
        multiple={multiple || false}
        hidden
        name="file-upload"
        accept={accept}
        onChange={onChange}
        {...rest}
        ref={(e) => {
          ref(e);
          inputRef.current = e;
        }}
      />
      <>{children}</>
    </InputGroup>
  );
};

export default FileUpload;
