import { Checkbox, CheckboxGroup, CheckboxGroupProps } from '@chakra-ui/checkbox';
import { FormControl, FormLabel } from '@chakra-ui/form-control';
import { VStack } from '@chakra-ui/layout';
import { FC } from 'react';

interface ICheckbox extends InputProps {
  options: string[];
}

export const CheckboxInput: FC<ICheckbox & CheckboxGroupProps> = (props) => {
  const { id, errors, rules, register, title, options, defaultValue, ...rest } = props;
  return (
    <FormControl isInvalid={errors[id]}>
      <FormLabel htmlFor={id}>{title}</FormLabel>
      <CheckboxGroup {...rest} defaultValue={defaultValue}>
        <VStack align="start">
          {options.map((item, index) => (
            <Checkbox key={index} name={id} {...register(id, rules)} value={item}>
              {item}
            </Checkbox>
          ))}
        </VStack>
      </CheckboxGroup>
    </FormControl>
  );
};
