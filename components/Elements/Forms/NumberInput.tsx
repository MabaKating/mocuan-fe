import { FormControl, FormErrorMessage, FormLabel } from '@chakra-ui/form-control';
import {
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
  UseNumberInputProps,
} from '@chakra-ui/number-input';
import { FC } from 'react';

export const NumberInputs: FC<InputProps & UseNumberInputProps> = (props) => {
  const { id, errors, rules, register, title, placeholder, ...rest } = props;

  return (
    <FormControl isInvalid={errors[id]}>
      {title && <FormLabel htmlFor={id}>{title}</FormLabel>}
      <NumberInput {...rest}>
        <NumberInputField {...register(id, rules)} id={id} placeholder={placeholder} />
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      </NumberInput>

      <FormErrorMessage>{errors[id] && errors[id].message}</FormErrorMessage>
    </FormControl>
  );
};
