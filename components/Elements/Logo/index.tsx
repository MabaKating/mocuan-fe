import { Box, Text, TextProps } from '@chakra-ui/layout';
import { FC } from 'react';

const Logo: FC<TextProps> = (props) => {
  return (
    <Text fontSize="xl" color="#118C4F" {...props}>
      <Box fontWeight={900} as="span">
        M
      </Box>
      <span>🤑</span>
      <Box fontWeight={900} as="span">
        Cuan
      </Box>
    </Text>
  );
};

export default Logo;
