import { Box, Flex } from '@chakra-ui/layout';
import { FC, useEffect, useState } from 'react';
import Link from 'next/link';
import { Button, IconButton } from '@chakra-ui/button';
import { useRouter } from 'next/router';
import { HamburgerIcon } from '@chakra-ui/icons';
import { useDisclosure } from '@chakra-ui/hooks';
import { Modal, ModalBody, ModalCloseButton, ModalContent, ModalOverlay } from '@chakra-ui/modal';
import Logo from 'components/Elements/Logo';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from 'config/store/reducers';
import { logout } from 'config/store/auth';

let NAVLINKS = [
  { name: 'Beranda', href: '/' },
  { name: 'Lowongan Kerja', href: '/lowongan-kerja' },
];

const RESTRICT_ROLE: { [key: string]: any } = {
  jobseeker: { name: 'Profil Saya', href: '/profile' },
  employer: { name: 'Perusahaan', href: '/perusahaan' },
  admin: { name: 'Dashboard', href: '/dashboard' },
};

export const PY = { base: '16px', md: 30 };
export const PX = { base: '16px', md: 120 };

const Navbar: FC = () => {
  return (
    <Flex top={0} justify="space-between" align="center" py={PY} px={PX} w="full">
      <Flex align="center" fontSize={{ base: 16, md: 24 }} justify="space-between" w="full">
        <Logo ml={1} />
        <LargeNavlink />
        <SmallNavlink />
      </Flex>
    </Flex>
  );
};

const LinkContent = () => {
  const router = useRouter();
  const auth = useSelector((state: RootState) => state.auth);
  const { token, user } = auth;
  const dispatch = useDispatch();
  const isCurrentPath = (url: string) => router.asPath === url;

  const performLogout = () => {
    router.replace('/');
    setTimeout(() => {
      dispatch(logout());
    }, 50);
  };

  const navbarLayout = (name: string, href: string) => {
    return (
      <Link key={name} href={href} passHref>
        <Box as="a" color="blue.700" fontWeight={isCurrentPath(href) ? 'semibold' : 'normal'}>
          {name}
        </Box>
      </Link>
    );
  };

  const AuthButton = token ? (
    <Button onClick={performLogout} colorScheme="red">
      Logout
    </Button>
  ) : (
    <Link href="/login">
      <a>
        <Button colorScheme="green">Login</Button>
      </a>
    </Link>
  );

  const [links, setLinks] = useState(NAVLINKS);

  useEffect(() => {
    if (token && user?.user_role) {
      const navItem = RESTRICT_ROLE[user.user_role];
      const isContain = links.some((item) => item.name === navItem.name);
      if (!isContain) {
        setLinks([...links, navItem]);
      }
    } else {
      setLinks(NAVLINKS);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [user?.user_role]);

  return (
    <>
      {links.map(({ name, href }) => navbarLayout(name, href))}
      {AuthButton}
    </>
  );
};

const LargeNavlink = () => {
  return (
    <Flex
      fontSize={{ base: 16, md: 18 }}
      justify="space-between"
      gridGap={4}
      align="center"
      display={{ base: 'none', lg: 'flex' }}
    >
      <LinkContent />
    </Flex>
  );
};

const SmallNavlink = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  return (
    <Box display={{ base: 'block', lg: 'none' }}>
      <IconButton onClick={onOpen} aria-label="hamburger" icon={<HamburgerIcon />} />
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalCloseButton />
          <ModalBody pt={8} display="flex" flexDir="column" alignItems="center" gridGap={5}>
            <LinkContent />
          </ModalBody>
        </ModalContent>
      </Modal>
    </Box>
  );
};

export default Navbar;
