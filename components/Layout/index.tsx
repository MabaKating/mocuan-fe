import { Box } from '@chakra-ui/layout';
import { FC } from 'react';
import Navbar from './Navbar';

const Layout: FC = ({ children }) => {
  return (
    <Box minH="100vh">
      <Navbar />
      {children}
    </Box>
  );
};

export default Layout;
