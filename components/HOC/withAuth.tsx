/* eslint-disable react/display-name */
import Loading from 'components/Elements/Loading';
import { isLoggedin } from 'config/store/auth/selector';
import { useRouter } from 'next/router';
import React from 'react';
import { useSelector } from 'react-redux';

export const withAuth = (WrappedComponent: any, options?: { pathAfterFailure: string }) => {
  return (props: JSX.IntrinsicAttributes) => {
    const loggedIn = useSelector(isLoggedin);
    const router = useRouter();
    if (typeof window !== undefined) {
      if (loggedIn) {
        return <WrappedComponent {...props} />;
      } else {
        router.push(options?.pathAfterFailure || '/');
        return <Loading />;
      }
    } else return <WrappedComponent {...props} />;
  };
};
