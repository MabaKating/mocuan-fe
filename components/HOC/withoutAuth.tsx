/* eslint-disable react/display-name */
import Loading from 'components/Elements/Loading';
import { isLoggedin } from 'config/store/auth/selector';
import { useRouter } from 'next/router';
import React from 'react';
import { useSelector } from 'react-redux';

export const withoutAuth = (WrappedComponent: any, options?: { pathAfterFailure: string }) => {
  return (props: JSX.IntrinsicAttributes) => {
    const loggedIn = useSelector(isLoggedin);
    const router = useRouter();
    if (typeof window !== undefined) {
      if (loggedIn) {
        router.push(options?.pathAfterFailure || '/');
        return <Loading />;
      } else {
        return <WrappedComponent {...props} />;
      }
    } else return <WrappedComponent {...props} />;
  };
};
