/* eslint-disable react/display-name */
import Loading from 'components/Elements/Loading';
import { RootState } from 'config/store/reducers';
import { useRouter } from 'next/router';
import React from 'react';
import { useSelector } from 'react-redux';

export const withAdminAuth = (WrappedComponent: any, options?: { pathAfterFailure: string }) => {
  return (props: JSX.IntrinsicAttributes) => {
    const user = useSelector((state: RootState) => state.auth.user);
    const router = useRouter();
    const isAdmin = user?.user_role === 'admin';
    if (typeof window !== undefined) {
      if (isAdmin) {
        return <WrappedComponent {...props} />;
      } else {
        router.push(options?.pathAfterFailure || '/');
        return <Loading />;
      }
    } else return <WrappedComponent {...props} />;
  };
};
