import { Button } from '@chakra-ui/button';
import { Box, Flex, Text } from '@chakra-ui/layout';
import { FC } from 'react';
import { useRouter } from 'next/router';
import { SubmitHandler, useForm } from 'react-hook-form';
import { PasswordInput, TextInput } from 'components/Elements/Forms';
import { withoutAuth } from 'components/HOC';
import axios from 'axios';
import ROUTE from 'config/api/route';
import { FailToast, SuccessToast } from 'components/Toast';
import { login } from 'config/store/auth';
import { useDispatch } from 'react-redux';

type FormValues = {
  identifier: string;
  password: string;
};

const LoginAdmin: FC = () => {
  const router = useRouter();
  const dispatch = useDispatch();

  const {
    handleSubmit,
    register,
    formState: { errors, isSubmitting },
  } = useForm<FormValues>();

  const fetchLogin = async (values: FormValues) => {
    try {
      const req = await axios.post(ROUTE.LOGIN_ADMIN, values);
      const { jwt, user } = await req.data;
      const { user_role } = user;

      dispatch(login({ jwt, user }));

      setTimeout(() => {
        router.replace('/dashboard');
      }, 250);

      SuccessToast({
        title: `Hi! ${user.username}`,
        desc: `Masuk sebagai ${user_role}`,
      });
    } catch (error) {
      FailToast({ title: 'Mohon Maaf 🙏', desc: 'Terdapat kesalahan' });
    }
  };

  const onSubmit: SubmitHandler<FormValues> = (values) => {
    return new Promise<void>(async (resolve) => {
      await fetchLogin(values);
      resolve();
    });
  };
  return (
    <Flex
      backgroundImage="url('/login/decorator.svg')"
      backgroundPosition="top center"
      minH="100vh"
      backgroundRepeat="no-repeat"
      justify="center"
      align="center"
      px={4}
    >
      <Box
        boxShadow="lg"
        borderRadius={10}
        p={4}
        w="full"
        h="full"
        maxW="600"
        maxH="800"
        bgColor="white"
        pos="relative"
      >
        <Button pos="absolute" onClick={() => router.back()}>
          🔙
        </Button>
        <Box
          as="img"
          src="https://cdn.statically.io/gl/MabaKating/cdn/main/login/admin10.png"
          mt="-80px"
          mx="auto"
          w="100px"
        />
        <Text fontWeight={700} align="center">
          Admin Login
        </Text>

        <Box as="form" mt={5} w="full" maxW="400px" mx="auto" onSubmit={handleSubmit(onSubmit)}>
          <TextInput
            id="identifier"
            register={register}
            placeholder="user_admin"
            title="Username / Email"
            errors={errors}
            rules={{
              required: 'Wajib Di isi',
              minLength: { value: 6, message: 'Minimal panjang string adalah 6' },
            }}
          />

          <PasswordInput
            id="password"
            register={register}
            errors={errors}
            title="Password"
            placeholder="********"
            rules={{
              required: 'Wajib diisi',
              minLength: { value: 8, message: 'Minimal panjang string adalah 8' },
            }}
            mt={4}
          />

          <Box w="fit-content" ml="auto">
            <Button mt={4} colorScheme="teal" isLoading={isSubmitting} type="submit">
              Submit
            </Button>
          </Box>
        </Box>
      </Box>
    </Flex>
  );
};

export default withoutAuth(LoginAdmin);
