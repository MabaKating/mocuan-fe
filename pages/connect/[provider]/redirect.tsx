import axios from 'axios';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { login } from 'config/store/auth';
import { FailToast, SuccessToast } from 'components/Toast';
import Loading from 'components/Elements/Loading';
import ROUTE from 'config/api/route';

const REDIRECT_PATH: { [key: string]: string } = {
  jobseeker: '/profile',
  employer: '/perusahaan',
};

const Redirect = () => {
  const router = useRouter();
  const dispatch = useDispatch();
  const {
    query: { provider, access_token },
  } = router;

  const getToken = async () => {
    try {
      const req = await axios.get(ROUTE.AUTH_CALLBACK(provider as string), {
        params: { access_token },
      });
      const { jwt, user } = await req.data;
      const { user_role } = user;

      dispatch(login({ jwt, user }));
      router.replace(`${!!user_role ? REDIRECT_PATH[user_role] : '/create-profile'}`);

      SuccessToast({
        title: `Hi! ${user.username}`,
        desc: `${user_role ? `Masuk sebagai ${user_role}` : 'Selamat bergabung 👋'}`,
      });
    } catch (error) {
      FailToast({ title: `Maaf email anda sudah terdaftar`, desc: 'Gunakan email yang berbeda' });
      router.replace('/login');
    }
  };

  useEffect(() => {
    if (provider && access_token) {
      getToken();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [access_token, provider]);
  return <Loading />;
};

export default Redirect;
