import React from 'react';
import Loading from '../../components/Elements/Loading';
import { useRouter } from 'next/router';
import { RootState } from '../../config/store/reducers';
import { useSelector } from 'react-redux';

export default function EditProfileSimple() {
  const username = useSelector((state: RootState) => state.auth.user)?.username;
  const router = useRouter();
  router.replace('/edit-profile/' + username);
  return <Loading />;
}
