import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  Box,
  Button,
  Center,
  Circle,
  Container,
  Divider,
  InputLeftAddon,
  Stack,
  Text,
  useColorModeValue,
  useToast,
} from '@chakra-ui/react';
import { Heading } from '@chakra-ui/layout';
import { FaCamera } from 'react-icons/fa';
import { TextInput } from '../../components/Elements/Forms';
import React, { ChangeEvent, useEffect, useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { ButtonGroup } from '@chakra-ui/button';
import { useDisclosure } from '@chakra-ui/hooks';
import {
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
} from '@chakra-ui/modal';
import { InputGroup } from '@chakra-ui/input';
import { TextInputNoTelpon } from 'components/Elements/Forms/TextInputNoTelpon';
import Navbar from '../../components/Layout/Navbar';
import { useRouter } from 'next/router';
import axios from 'axios';
import ROUTE from '../../config/api/route';
import { Spinner } from '@chakra-ui/spinner';
import { FailToast, SuccessToast } from '../../components/Toast';
import { useSelector } from 'react-redux';
import { RootState } from '../../config/store/reducers';
import config from '../../config/api/domain';

type FormValues = {
  namaDepan: string;
  namaBelakang: string;
  id: string;
  jid: string;
  username: string;
  kota: string;
  provinsi: string;
  pendidikan1: string;
  pendidikan2: string;
  pendidikan3: string;
  pengalaman1: string;
  pengalaman2: string;
  pengalaman3: string;
  keahlian1: string;
  keahlian2: string;
  keahlian3: string;
  nomorTelpon: string;
};

export default function EditProfileDetail() {
  const [isLoadingStateCV, setLoadingStateCV] = useState(false);
  const [jobs, setJobs] = useState<User[]>([]);
  const [isLoading, setLoading] = useState(true);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [files, setFiles] = useState<FileList | null>();
  const [filePath, setFilesPath] = useState<string | null | undefined>();
  const router = useRouter();
  const id = useSelector((state: RootState) => state.auth.user)?.id;
  const bgbox = useColorModeValue('white', 'gray.900');
  const fetchJobs = async (params = {}) => {
    try {
      setLoading(true);
      const req = await axios.get(ROUTE.USERS, { params: { username: username, ...params } });
      const res = req.data;
      setJobs(res);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };
  useEffect(() => {
    fetchJobs();
  }, []);
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();
  if (id == 0) {
    router.replace('/');
    return FailToast({
      title: 'Tidak Bisa Diakses',
      desc: '',
    });
  }

  const onSubmit: SubmitHandler<FormValues> = (values) => {
    return new Promise<void>(async () => {
      const jobseeker = {
        nomor_telepon: values.nomorTelpon,
        pendidikan: `${values.pendidikan1};${values.pendidikan2};${values.pendidikan3}`,
        skills: `${values.keahlian1};${values.keahlian2};${values.keahlian3}`,
        pengalaman: `${values.pengalaman1};${values.pengalaman2};${values.pengalaman3}`,
        lokasi: `${values.kota};${values.provinsi}`,
      };
      await axios.put(ROUTE.JOBSEEKER(values.jid), jobseeker);
      const user = {
        nama_depan: values.namaDepan,
        nama_belakang: values.namaBelakang,
      };
      await axios.put(ROUTE.USER(values.id), user);
      router.replace('/profile');
      SuccessToast({
        title: `Berhasil disimpan`,
        desc: '',
      });
    });
  };
  let username = '';
  if (typeof window !== 'undefined') {
    const temp_list = window.location.toString().split('/');
    username = temp_list[temp_list.length - 1];
  }

  const handleChangeProfPic = async (event: ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files![0];
    if (file) {
      var profPicEl = document.getElementById('profpic')!;
      var profile_picture = profPicEl.children[0] as HTMLImageElement;
      const formData = new FormData();
      formData.append('files', file);
      const req = await axios.get(ROUTE.USER(id + ''));
      const jid = req.data.jobseeker?.id;
      const isOnline = window.navigator.onLine;
      if (!isOnline) {
        FailToast({
          title: 'Gagal Mengganti Foto',
          desc: '',
        });
        onClose();
        return;
      }
      axios
        .post(ROUTE.UPLOAD, formData)
        .then((response) => {
          const imageId = response.data[0].id;
          axios
            .put(ROUTE.JOBSEEKER(jid + ''), { profile_picture: imageId })
            .then((response) => {
              profile_picture.src = config.API_BASE_URL + response.data.profile_picture.url;
              SuccessToast({
                title: 'Berhasil Mengganti Foto',
                desc: '',
              });
            })
            .catch(() => {});
        })
        .catch(() => {
          FailToast({
            title: 'Gagal Mengganti Foto',
            desc: '',
          });
        });
    }
  };

  const handleUploadCV = async () => {
    const formData = new FormData();
    const req = await axios.get(ROUTE.USER(id + ''));
    const jid = req.data.jobseeker?.id;
    formData.append('files', files![0]);
    setLoadingStateCV(true);
    const isOnline = window.navigator.onLine;
    if (!isOnline) {
      FailToast({
        title: 'Gagal Mengunggah CV',
        desc: '',
      });
      setLoadingStateCV(false);
      onClose();
      return;
    }
    axios
      .post(ROUTE.UPLOAD, formData)
      .then((response) => {
        const fileId = response.data[0].id;
        axios
          .put(ROUTE.JOBSEEKER(jid + ''), { curriculum_vitae: fileId })
          .then((response) => {
            const fileName = filePath!.split('\\')[2];
            const fileNameTruncated =
              fileName.length > 30 ? fileName.substring(0, 30 - 3) + '...' : fileName;
            document.getElementById('cvname')!.innerText = fileNameTruncated;
            SuccessToast({
              title: 'Berhasil Mengunggah CV',
              desc: '',
            });
            setLoadingStateCV(false);
          })
          .catch((error) => {});
      })
      .catch((error) => {
        FailToast({
          title: 'Gagal Mengunggah CV',
          desc: '',
        });
        setLoadingStateCV(false);
      });
    onClose();
  };

  return (
    <Box minH={'100vh'}>
      <Navbar />
      <Center py={6}>
        <Box
          maxW={'350px'}
          w={'full'}
          bg={bgbox}
          boxShadow={'2xl'}
          overflow={'initial'}
          rounded={'lg'}
          p={6}
          textAlign={'center'}
        >
          {isLoading ? (
            <Spinner
              mt={5}
              thickness="4px"
              speed="0.65s"
              emptyColor="gray.200"
              color="#7928CA"
              size="xl"
            />
          ) : (
            jobs.map((profile, index) => (
              <form key={profile.id} onSubmit={handleSubmit(onSubmit)}>
                <Container pos={'relative'}>
                  <Avatar
                    id="profpic"
                    size={'xl'}
                    bg={'white'}
                    border="1px"
                    borderColor="red"
                    src={
                      profile.jobseeker?.profile_picture?.url == null
                        ? 'https://static.thenounproject.com/png/3134331-200.png'
                        : config.API_BASE_URL + profile.jobseeker?.profile_picture?.url
                    }
                    alt={'Avatar Alt'}
                    mb={4}
                    pos={'relative'}
                  />
                  <Container
                    margin-left={'auto'}
                    left={'30px'}
                    right={'0px'}
                    margin-right={'auto'}
                    pos={'absolute'}
                    width={'30px'}
                    bottom={'10px'}
                  >
                    <label htmlFor={'profilePic'}>
                      <Circle
                        as={'span'}
                        bg={'white'}
                        size={'30px'}
                        border="1px"
                        borderColor="gray.200"
                        cursor={'pointer'}
                      >
                        <FaCamera color={'teal'}></FaCamera>
                      </Circle>
                    </label>
                    <input
                      accept={'image/*'}
                      type={'file'}
                      id={'profilePic'}
                      hidden
                      onChange={handleChangeProfPic}
                    />
                  </Container>
                </Container>
                <Accordion allowMultiple>
                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          <Heading fontSize={'xl'} fontFamily={'body'} textAlign={'left'}>
                            Informasi Umum
                          </Heading>
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      <TextInput
                        id="namaDepan"
                        defaultValue={profile.nama_depan}
                        title="Nama Depan"
                        register={register}
                        errors={errors}
                        rules={{
                          required: 'Wajib diisi',
                        }}
                      />
                      <Stack marginTop={'5px'} marginBottom={'5px'}></Stack>
                      <TextInput
                        id="namaBelakang"
                        defaultValue={profile.nama_belakang}
                        title="Nama Belakang"
                        register={register}
                        errors={errors}
                        rules={{}}
                      />
                      <Stack marginTop={'5px'} marginBottom={'5px'}></Stack>
                      <TextInput
                        id="kota"
                        defaultValue={profile.jobseeker?.lokasi.split(';')[0]}
                        title="Kota"
                        register={register}
                        errors={errors}
                        rules={{}}
                      />
                      <Stack marginTop={'5px'} marginBottom={'5px'}></Stack>
                      <TextInput
                        id="provinsi"
                        defaultValue={profile.jobseeker?.lokasi.split(';')[1]}
                        title="Provinsi"
                        register={register}
                        errors={errors}
                        rules={{}}
                      />
                    </AccordionPanel>
                  </AccordionItem>

                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          <Heading fontSize={'xl'} fontFamily={'body'} textAlign={'left'}>
                            Kontak
                          </Heading>
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      <InputGroup isInvalid={errors['nomorTelpon']}>
                        <InputLeftAddon>+62</InputLeftAddon>
                        <TextInputNoTelpon
                          id="nomorTelpon"
                          placeholder={'Nomor Telpon'}
                          register={register}
                          errors={errors}
                          defaultValue={
                            profile.jobseeker?.nomor_telepon == null ||
                            profile.jobseeker?.nomor_telepon + '' == '0'
                              ? ''
                              : profile.jobseeker?.nomor_telepon + ''
                          }
                          rules={{
                            validate: {
                              value: (value: string) => {
                                if (value.length == 0) {
                                  return true;
                                }
                                if (value.toLowerCase() == value.toUpperCase()) {
                                  return true;
                                } else {
                                  return 'Nomor telpon tidak valid';
                                }
                              },
                            },
                            minLength: { value: 10, message: 'Nomor telpon tidak valid' },
                            maxLength: { value: 13, message: 'Nomor telpon tidak valid' },
                          }}
                        />
                      </InputGroup>
                    </AccordionPanel>
                  </AccordionItem>

                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          <Heading fontSize={'xl'} fontFamily={'body'}>
                            Pendidikan
                          </Heading>
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      {profile.jobseeker?.pendidikan != null
                        ? profile.jobseeker?.pendidikan
                            .split(';')
                            .map((item, index) => (
                              <TextInput
                                key={item}
                                id={'pendidikan' + (index + 1)}
                                title={'Pendidikan ' + (index + 1)}
                                register={register}
                                errors={errors}
                                defaultValue={item}
                              />
                            ))
                        : [0, 1, 2].map((item, index) => (
                            <TextInput
                              key={item}
                              id={'pendidikan' + (index + 1)}
                              title={'Pendidikan ' + (index + 1)}
                              register={register}
                              errors={errors}
                            />
                          ))}
                    </AccordionPanel>
                  </AccordionItem>

                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          <Heading fontSize={'xl'} fontFamily={'body'}>
                            Pengalaman
                          </Heading>
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      {profile.jobseeker?.pengalaman != null
                        ? profile.jobseeker?.pengalaman
                            .split(';')
                            .map((item, index) => (
                              <TextInput
                                key={item}
                                id={'pengalaman' + (index + 1)}
                                title={'Pengalaman ' + (index + 1)}
                                register={register}
                                errors={errors}
                                defaultValue={item}
                              />
                            ))
                        : [0, 1, 2].map((item, index) => (
                            <TextInput
                              key={item}
                              id={'pengalaman' + (index + 1)}
                              title={'Pengalaman ' + (index + 1)}
                              register={register}
                              errors={errors}
                            />
                          ))}
                    </AccordionPanel>
                  </AccordionItem>

                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          <Heading fontSize={'xl'} fontFamily={'body'}>
                            Keahlian
                          </Heading>
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      {profile.jobseeker?.skills != null
                        ? profile.jobseeker?.skills
                            .split(';')
                            .map((item1, index) => (
                              <TextInput
                                key={item1}
                                id={'keahlian' + (index + 1)}
                                title={'Keahlian ' + (index + 1)}
                                register={register}
                                errors={errors}
                                defaultValue={item1}
                              />
                            ))
                        : [0, 1, 2].map((item1, index) => (
                            <TextInput
                              key={item1}
                              id={'keahlian' + (index + 1)}
                              title={'Keahlian ' + (index + 1)}
                              register={register}
                              errors={errors}
                            />
                          ))}
                    </AccordionPanel>
                  </AccordionItem>
                </Accordion>
                <input
                  type="hidden"
                  {...register('username')}
                  id={'username'}
                  value={profile.username}
                ></input>
                <input type="hidden" {...register('id')} id={'id'} value={profile.id}></input>
                <input
                  type="hidden"
                  {...register('jid')}
                  id={'jid'}
                  value={profile.jobseeker?.id}
                ></input>
                <Stack marginTop={'10px'} marginBottom={'10px'}></Stack>
                <input
                  accept={'application/pdf'}
                  hidden
                  id="cv"
                  type="file"
                  onChange={function (e) {
                    onOpen();
                    setFilesPath(e.target.value);
                    return setFiles(e.target.files);
                  }}
                />
                <Text id="cvname" mb={'4px'}>
                  {profile.jobseeker?.curriculum_vitae?.name == null
                    ? 'None'
                    : profile.jobseeker?.curriculum_vitae?.name?.length > 30
                    ? profile.jobseeker?.curriculum_vitae!.name.substring(0, 30 - 3) + '...'
                    : profile.jobseeker?.curriculum_vitae!.name}
                </Text>
                <ButtonGroup>
                  <label htmlFor={'cv'}>
                    <Button
                      as={'text'}
                      colorScheme={'blue'}
                      variant="outline"
                      cursor={'pointer'}
                      size={'md'}
                      isLoading={isLoadingStateCV}
                      loadingText={'Mengunggah'}
                    >
                      Unggah CV
                    </Button>
                  </label>
                  <HapusCVButton />
                </ButtonGroup>
                <Stack marginTop={'10px'} marginBottom={'10px'}>
                  <Divider></Divider>
                </Stack>
                <Stack mt={8} direction={'row'} spacing={4}>
                  <Button
                    type={'submit'}
                    flex={1}
                    fontSize={'md'}
                    rounded={'full'}
                    colorScheme={'teal'}
                    boxShadow={
                      '0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)'
                    }
                  >
                    Simpan
                  </Button>
                  <Button
                    flex={1}
                    fontSize={'md'}
                    rounded={'full'}
                    colorScheme={'teal'}
                    variant={'outline'}
                    border={'grey solid 1px'}
                    as="a"
                    href={'../profile/' + username}
                    boxShadow={
                      '0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)'
                    }
                  >
                    Batal
                  </Button>
                </Stack>
              </form>
            ))
          )}
        </Box>

        <Modal onClose={onClose} size={'sm'} isOpen={isOpen}>
          <ModalOverlay />
          <ModalContent>
            <ModalHeader textAlign={'center'}>Hapus CV</ModalHeader>
            <ModalCloseButton />
            <ModalBody textAlign={'center'}>Apakah Anda yakin untuk mengupload CV?</ModalBody>
            <ModalFooter display={'flex'} justifyContent={'space-evenly'}>
              <Button size="lg" onClick={handleUploadCV} colorScheme={'teal'}>
                Iya
              </Button>
              <Button size="lg" onClick={onClose} colorScheme={'teal'} variant={'outline'}>
                Batal
              </Button>
            </ModalFooter>
          </ModalContent>
        </Modal>
      </Center>
    </Box>
  );
}

function HapusCVButton() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [size, setSize] = React.useState('sm');

  const handleSizeClick = ({ newSize }: { newSize: any }) => {
    setSize(newSize);
    onOpen();
  };

  const sizes = ['sm'];
  const toast = useToast();

  const deleteCV = async () => {
    const el = document.getElementById('jid') as HTMLInputElement;
    const jid = el.value;
    const isOnline = window.navigator.onLine;
    if (!isOnline) {
      FailToast({
        title: 'Gagal Menghapus CV',
        desc: '',
      });
      onClose();
      return;
    }
    const req = await axios.get(ROUTE.JOBSEEKER(jid));
    try {
      const fid = req.data.curriculum_vitae!.id;
      await axios
        .delete(ROUTE.UPLOAD_ID(fid))
        .then(() => {
          (document.getElementById('cvname') as HTMLSpanElement).innerText = 'None';
          SuccessToast({
            title: 'Berhasil Menghapus CV',
            desc: '',
          });
        })
        .catch(() => {
          FailToast({
            title: 'Gagal Menghapus CV',
            desc: '',
          });
        });
    } catch (e) {
      toast({
        title: 'Anda belum mengunggah CV',
        description: '',
        status: 'info',
        isClosable: true,
      });
    }
    onClose();
  };

  return (
    <>
      {sizes.map((size) => (
        <Button
          colorScheme={'red'}
          variant={'outline'}
          size={'md'}
          onClick={() => handleSizeClick({ newSize: size })}
          key={size}
        >
          Hapus CV
        </Button>
      ))}

      <Modal onClose={onClose} size={size} isOpen={isOpen}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader textAlign={'center'}>Hapus CV</ModalHeader>
          <ModalCloseButton />
          <ModalBody textAlign={'center'}>Apakah Anda yakin untuk menghapus CV?</ModalBody>
          <ModalFooter display={'flex'} justifyContent={'space-evenly'}>
            <Button size="lg" onClick={onClose} variant={'outline'}>
              Batal
            </Button>
            <Button size="lg" onClick={deleteCV} colorScheme={'red'}>
              Iya
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
}
