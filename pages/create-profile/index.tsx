import { Box, Button, Center, Select, Stack, Text, useColorModeValue } from '@chakra-ui/react';
import { Heading } from '@chakra-ui/layout';
import React from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { TextInput } from '../../components/Elements/Forms';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../config/store/reducers';
import ROUTE from '../../config/api/route';
import { SuccessToast } from '../../components/Toast';
import { useRouter } from 'next/router';
import { updateUser } from 'config/store/auth';

type FormValues = {
  role: string;
  namaDepan: string;
  namaBelakang: string;
  id: string;
  username: string;
};

export default function SocialProfileSimple() {
  const router = useRouter();
  const dispatch = useDispatch();
  const onSubmit: SubmitHandler<FormValues> = (values) => {
    return new Promise<void>(async () => {
      if (values.role == 'jobseeker') {
        const res1 = await axios.post(ROUTE.JOBSEEKERS, {
          pengalaman: ';;',
          skills: ';;',
          pendidikan: ';;',
          lokasi: ';',
        });
        const user = {
          user_role: values.role,
          username: values.username,
          nama_depan: values.namaDepan,
          nama_belakang: values.namaBelakang,
          jobseeker: res1.data.id,
        };
        const userRegistered = await axios.put(ROUTE.USER(values.id), user);
        dispatch(updateUser(userRegistered.data));

        setTimeout(() => {
          router.replace('/profile');
        }, 250);
        SuccessToast({
          title: `Hi! ${user.username}`,
          desc: `Masuk sebagai ${user.user_role}`,
        });
      } else {
        const req = await axios.post(ROUTE.EMPLOYERS, { pengalaman: null });
        const user = {
          user_role: values.role,
          username: values.username,
          nama_depan: values.namaDepan,
          nama_belakang: values.namaBelakang,
          employer: req.data.id,
        };
        const userRegistered = await axios.put(ROUTE.USER(values.id), user);
        dispatch(updateUser(userRegistered.data));
        router.replace('/perusahaan');
        SuccessToast({
          title: `Hi! ${user.username}`,
          desc: `Masuk sebagai ${user.user_role}`,
        });
      }
    });
  };
  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm();

  const id = useSelector((state: RootState) => state.auth.user)?.id;
  const username = useSelector((state: RootState) => state.auth.user)?.username;
  return (
    <Center py={6}>
      <Box
        maxW={'320px'}
        w={'full'}
        bg={useColorModeValue('white', 'gray.900')}
        boxShadow={'2xl'}
        overflow={'initial'}
        rounded={'lg'}
        p={6}
        textAlign={'center'}
      >
        <form onSubmit={handleSubmit(onSubmit)}>
          <Heading as={'h1'}>Sign Up</Heading>
          <Stack marginTop={'5px'} marginBottom={'5px'}></Stack>
          <Text marginBottom="7px" fontWeight={'500'} textAlign={'left'} as={'h6'} size={'sm'}>
            Role
          </Text>
          <input type="hidden" {...register('id')} id={'id'} value={id}></input>
          <input type="hidden" {...register('username')} id={'username'} value={username}></input>
          <Select id="role" {...register('role')} placeholder="Select option">
            <option value="jobseeker">Job Seeker</option>
            <option value="employer">Employer</option>
          </Select>
          <Stack marginTop={'5px'} marginBottom={'5px'}></Stack>
          <TextInput
            id="namaDepan"
            title="Nama Depan"
            register={register}
            errors={errors}
            rules={{
              required: 'Wajib diisi',
              minLength: { value: 3, message: 'Minimum length should be 3' },
            }}
          />
          <Stack marginTop={'5px'} marginBottom={'5px'}></Stack>
          <TextInput
            id="namaBelakang"
            title="Nama Belakang"
            register={register}
            errors={errors}
            rules={{
              required: 'Wajib diisi',
              minLength: { value: 3, message: 'Minimum length should be 3' },
            }}
          />
          <Stack mt={8} direction={'row'} spacing={4}>
            <Button
              type={'submit'}
              flex={1}
              fontSize={'md'}
              rounded={'full'}
              colorScheme={'teal'}
              boxShadow={
                '0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)'
              }
            >
              Simpan
            </Button>
          </Stack>
        </form>
      </Box>
    </Center>
  );
}
