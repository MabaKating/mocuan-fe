// @ts-nocheck
import Layout from 'components/Layout';
import DetailPerusahaan from 'components/Container/Perusahaan/DetailPerusahaan';
import axios from 'axios';
import { useRouter } from 'next/router';
import ROUTE from 'config/api/route';
import Loading from 'components/Elements/Loading';
import { useState } from 'react';
import { useEffect } from 'react';

const CompanyProfileDetail = () => {
  const router = useRouter();
  const { id } = router.query;

  const [state, setstate] = useState();

  const fetchCompanyProfile = async () => {
    try {
      const res = await axios.get(ROUTE.COMPANY(id));
      setstate(res.data);
    } catch (error) {}
  };

  useEffect(() => {
    if (id) {
      fetchCompanyProfile();
    }
  }, [id]);

  return <Layout>{state ? <DetailPerusahaan company={state} /> : <Loading />}</Layout>;
};

export default CompanyProfileDetail;
