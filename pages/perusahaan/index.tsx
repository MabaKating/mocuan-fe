// @ts-nocheck
import axios from 'axios';
import React from 'react';
import PageBuatPerusahaan from 'components/Container/Perusahaan/PageBuatPerusahaan';
import DetailPerusahaan from 'components/Container/Perusahaan/DetailPerusahaan';
import Layout from 'components/Layout';
import { useSelector } from 'react-redux';
import { useState, useEffect } from 'react';
import { RootState } from 'config/store/reducers';
import ErrorPage from 'next/error';
import { Spinner } from '@chakra-ui/spinner';
import { HStack } from '@chakra-ui/layout';
import ROUTE from 'config/api/route';

export default function Company() {
  const [company, setCompany] = React.useState(null);
  const [jobs, setJobs] = React.useState(null);
  const [isLoading, setLoading] = useState(true);
  const auth = useSelector((state: RootState) => state.auth);

  const fetchEmployer = async () => {
    try {
      const { data } = await axios.get(ROUTE.EMPLOYER(auth.user?.employer?.id)).then((response) => {
        const companyData = response.data.company;
        const jobsData = response.data.jobs;
        if (companyData != null) {
          setCompany(companyData);
          setJobs(jobsData);
        }
      });
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchEmployer();
  }, []);

  if (auth.user?.user_role != 'employer') {
    return <ErrorPage statusCode={404} />;
  }

  return (
    <Layout>
      {isLoading ? (
        <HStack mt={6}>
          <Spinner
            mt={2}
            mx="auto"
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="#7928CA"
            size="xl"
          />
        </HStack>
      ) : company == null ? (
        <PageBuatPerusahaan setCompany={setCompany} />
      ) : (
        <DetailPerusahaan company={company} user={auth.user} setCompany={setCompany} jobs={jobs} />
      )}
    </Layout>
  );
}
