import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Avatar,
  Box,
  Button,
  Center,
  Container,
  Heading,
  Link,
  List,
  ListIcon,
  ListItem,
  Stack,
  Text,
  Flex,
  useColorModeValue,
} from '@chakra-ui/react';
import fileDownload from 'js-file-download';
import { DownloadIcon, EditIcon, EmailIcon, PhoneIcon, SmallAddIcon } from '@chakra-ui/icons';
import Navbar from '../../components/Layout/Navbar';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import ROUTE from '../../config/api/route';
import { Spinner } from '@chakra-ui/spinner';
import { useSelector } from 'react-redux';
import { RootState } from '../../config/store/reducers';
import config from '../../config/api/domain';
import NextLink from 'next/link';

export default function ProfileDetail() {
  const id = useSelector((state: RootState) => state.auth.user)?.id;
  const isAuth = id == 0 ? false : true;
  let username = '';
  if (typeof window !== 'undefined') {
    const temp_list = window.location.toString().split('/');
    username = temp_list[temp_list.length - 1];
  }
  const [jobs, setJobs] = useState<User[]>([]);
  const [isLoading, setLoading] = useState(true);
  const fetchJobs = async (params = {}) => {
    try {
      setLoading(true);
      const req = await axios.get(ROUTE.USERS, { params: { username: username, ...params } });
      const res = req.data;
      setJobs(res);
    } catch (error) {
    } finally {
      setLoading(false);
    }
  };

  const bgcolor = useColorModeValue('white', 'gray.900');

  useEffect(() => {
    fetchJobs();
  }, []);

  return (
    <Box minH={'100vh'}>
      <Navbar />
      <Center>
        {isLoading ? (
          <Spinner
            mt={5}
            thickness="4px"
            speed="0.65s"
            emptyColor="gray.200"
            color="#7928CA"
            size="xl"
          />
        ) : (
          jobs.map((profile) => (
            <Box
              key={profile.id}
              maxW={'350px'}
              w={'full'}
              bg={bgcolor}
              boxShadow={'2xl'}
              overflow={'initial'}
              rounded={'lg'}
              p={6}
              position={'relative'}
              textAlign={'center'}
            >
              {isAuth ? (
                profile.id == id + '' ? (
                  <Link as={NextLink} href="/edit-profile" passHref>
                    <Flex
                      alignItems="center"
                      gridGap={1}
                      as="a"
                      position={'absolute'}
                      color={'blue.600'}
                      right={6}
                      top={'22px'}
                      verticalAlign={'top'}
                    >
                      <EditIcon /> Edit
                    </Flex>
                  </Link>
                ) : null
              ) : null}
              <Avatar
                size={'xl'}
                bg={'white'}
                border="1px"
                src={
                  profile.jobseeker?.profile_picture?.url == null
                    ? 'https://static.thenounproject.com/png/3134331-200.png'
                    : config.API_BASE_URL + profile.jobseeker?.profile_picture?.url
                }
                alt={'Avatar Alt'}
                mb={4}
                pos={'relative'}
              />
              <Heading fontSize={'2xl'} fontFamily={'body'}>
                {profile.nama_depan + ' ' + profile.nama_belakang}
              </Heading>
              <Text>{'@' + profile.username}</Text>
              <Text color={'gray.500'} mb={4}>
                {profile.jobseeker?.lokasi.split(';')[0] +
                  ', ' +
                  profile.jobseeker?.lokasi.split(';')[1]}
              </Text>
              <Container textAlign={'left'}>
                <List spacing={0}>
                  {profile.email == null ? null : isAuth ? (
                    <ListItem>
                      <ListIcon as={EmailIcon} color="green.500" />
                      {profile.email}
                    </ListItem>
                  ) : null}
                  {profile.jobseeker?.nomor_telepon == null ||
                  profile.jobseeker?.nomor_telepon + '' == '0' ? null : isAuth ? (
                    <ListItem>
                      <ListIcon as={PhoneIcon} color="green.500" />
                      {profile.jobseeker?.nomor_telepon}
                    </ListItem>
                  ) : null}
                </List>
                <Stack marginTop={'10px'} marginBottom={'10px'}></Stack>
                <Accordion allowMultiple>
                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          <Heading fontSize={'xl'} fontFamily={'body'} textAlign={'left'}>
                            Pendidikan
                          </Heading>
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      <List spacing={0}>
                        {profile.jobseeker?.pendidikan.split(';').map((item) => (
                          <ListItem key={item}>
                            <ListIcon as={SmallAddIcon} color="green.500" />
                            {item}
                          </ListItem>
                        ))}
                      </List>
                    </AccordionPanel>
                  </AccordionItem>

                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          <Heading fontSize={'xl'} fontFamily={'body'} textAlign={'left'}>
                            Pengalaman
                          </Heading>
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      <List spacing={0}>
                        {profile.jobseeker?.pengalaman.split(';').map((item) => (
                          <ListItem key={item}>
                            <ListIcon as={SmallAddIcon} color="green.500" />
                            {item}
                          </ListItem>
                        ))}
                      </List>
                    </AccordionPanel>
                  </AccordionItem>

                  <AccordionItem>
                    <h2>
                      <AccordionButton>
                        <Box flex="1" textAlign="left">
                          <Heading fontSize={'xl'} fontFamily={'body'} textAlign={'left'}>
                            Keahlian
                          </Heading>
                        </Box>
                        <AccordionIcon />
                      </AccordionButton>
                    </h2>
                    <AccordionPanel pb={4}>
                      <List spacing={0}>
                        {profile.jobseeker?.skills.split(';').map((item) => (
                          <ListItem key={item}>
                            <ListIcon as={SmallAddIcon} color="green.500" />
                            {item}
                          </ListItem>
                        ))}
                      </List>
                    </AccordionPanel>
                  </AccordionItem>
                </Accordion>
              </Container>

              {profile.jobseeker?.curriculum_vitae?.url == undefined ? null : (
                <Button
                  mt={8}
                  direction={'row'}
                  spacing={4}
                  flex={1}
                  size={'lg'}
                  fontSize={'lg'}
                  rounded={'full'}
                  colorScheme={'teal'}
                  boxShadow={
                    '0px 1px 25px -5px rgb(66 153 225 / 48%), 0 10px 10px -5px rgb(66 153 225 / 43%)'
                  }
                  onClick={() => {
                    const url = profile.jobseeker?.curriculum_vitae?.url as string;
                    axios
                      .get(url, {
                        responseType: 'blob',
                      })
                      .then((res) => {
                        fileDownload(res.data, username + '_cv.pdf');
                      });
                  }}
                >
                  <DownloadIcon marginRight={'6px'} /> Download CV
                </Button>
              )}
            </Box>
          ))
        )}
      </Center>
    </Box>
  );
}
