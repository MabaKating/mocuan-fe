import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../config/store/reducers';
import Loading from '../../components/Elements/Loading';
import { useRouter } from 'next/router';

export default function SocialProfileSimple() {
  const username = useSelector((state: RootState) => state.auth.user)?.username;
  const router = useRouter();
  router.replace('/profile/' + username);
  return <Loading />;
}
