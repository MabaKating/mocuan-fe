import Dashboard from 'components/Container/Dashboard';
import { withAdminAuth } from 'components/HOC';
import Layout from 'components/Layout';

const DashboardPage = () => {
  return (
    <Layout>
      <Dashboard />
    </Layout>
  );
};

export default withAdminAuth(DashboardPage, { pathAfterFailure: '/login-admin' });
