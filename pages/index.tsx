import Homepage from 'components/Container/Homepage';
import Layout from 'components/Layout';
import type { NextPage } from 'next';

const Home: NextPage = () => {
  return (
    <Layout>
      <Homepage />
    </Layout>
  );
};

export default Home;
