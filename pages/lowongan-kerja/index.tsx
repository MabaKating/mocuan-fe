import Layout from 'components/Layout';
import Jobpost from 'components/Container/JobPost';

const LowonganKerja = () => {
  return (
    <Layout>
      <Jobpost />
    </Layout>
  );
};

export default LowonganKerja;
