import CreateJobPost from 'components/Container/CreateJobPost';
import Layout from 'components/Layout';

const CreatePage = () => {
  return (
    <Layout>
      <CreateJobPost />
    </Layout>
  );
};

export default CreatePage;
