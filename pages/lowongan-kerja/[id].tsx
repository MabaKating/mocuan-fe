import Layout from 'components/Layout';
import JobPostDetail from 'components/Container/JobPostDetail';
import axios from 'axios';
import { useRouter } from 'next/router';
import ROUTE from 'config/api/route';
import { useEffect, useState } from 'react';

const LowonganKerjaDetail = () => {
  const router = useRouter();
  const { id } = router.query;
  const [data, setData] = useState();

  const fetchData = async () => {
    try {
      const res = await axios.get(ROUTE.JOB(id as string));
      setData(res.data);
    } catch (error) {}
  };

  useEffect(() => {
    if (id) {
      fetchData();
    }
  }, [id]);

  return (
    <Layout>
      <JobPostDetail jobPostData={data} />
    </Layout>
  );
};

export default LowonganKerjaDetail;
