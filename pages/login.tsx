import { FC } from 'react';
import { Box, Flex, Link, Text, VStack } from '@chakra-ui/layout';
import Logo from 'components/Elements/Logo';
import { Button } from '@chakra-ui/button';
import { FaDiscord, FaGoogle, FaLinkedin } from 'react-icons/fa';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import config from 'config/api/domain';
import { withoutAuth } from 'components/HOC';

const Login: FC = () => {
  const router = useRouter();

  return (
    <Flex
      backgroundImage="url('/login/decorator.svg')"
      backgroundPosition="top center"
      minH="100vh"
      backgroundRepeat="no-repeat"
      justify="center"
      align="center"
      px={4}
    >
      <Box
        boxShadow="lg"
        borderRadius={10}
        p={4}
        w="full"
        h="full"
        maxW="600"
        maxH="800"
        bgColor="white"
        pos="relative"
      >
        <Button pos="absolute" onClick={() => router.back()}>
          🔙
        </Button>
        <Box
          as="img"
          src="https://cdn.statically.io/gl/MabaKating/cdn/main/login/lock.png"
          mt="-80px"
          mx="auto"
        />

        <Flex fontSize="xl" mt={8} align="center" gridGap={1} mx="auto" w="fit-content">
          <Text fontWeight="bold">Login ke</Text>
          <Logo />
        </Flex>
        <VStack align="stretch" w="full" maxW={300} mx="auto" my={5}>
          <a href={`${config.API_BASE_URL}/connect/google`}>
            <Button w="full" colorScheme="red" leftIcon={<FaGoogle />}>
              Google
            </Button>
          </a>
          <a href={`${config.API_BASE_URL}/connect/linkedin`}>
            <Button w="full" colorScheme="linkedin" leftIcon={<FaLinkedin />}>
              LinkedIn
            </Button>
          </a>
          <a href={`${config.API_BASE_URL}/connect/discord`}>
            <Button w="full" colorScheme="facebook" leftIcon={<FaDiscord />}>
              Discord
            </Button>
          </a>
        </VStack>
        <Box mx="auto" w="fit-content" color="teal.500" fontWeight={600}>
          <Link as={NextLink} href="/login-admin">
            Login sebagai Admin?
          </Link>
        </Box>
      </Box>
    </Flex>
  );
};

export default withoutAuth(Login);
