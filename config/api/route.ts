const ROUTE = {
  UPLOAD: '/upload',
  UPLOAD_ID: (id: string) => `/upload/files/${id}`,
  USERS: '/users',
  EMPLOYERS: '/employers',
  AUTH_CALLBACK: (provider: string) => `/auth/${provider}/callback`,
  LOGIN_ADMIN: '/auth/local',

  CURRENT_USER: '/users/me',
  JOBS: '/jobs',
  JOB: (id: string) => `/jobs/${id}`,

  COMPANIES: '/companies/',
  COMPANY: (id: string) => `/companies/${id}`,

  EMPLOYER: (id: string) => `/employers/${id}`,
  JOBSEEKERS: '/joobseekers',
  JOBSEEKER: (id: string) => `/joobseekers/${id}`,
  APPLICATIONS: '/applications',
  APPLICATION: (id: string) => `/applications/${id}`,

  USER: (id: string) => `/users/${id}`,
};

export default ROUTE;
