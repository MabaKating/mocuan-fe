let url;

if (typeof window !== 'undefined') {
  url = window.location.origin;
}

const config: { [key: string]: any } = {
  base: {
    DOMAIN: url,
  },
  production: {
    API_BASE_URL: 'https://mocuan-be.herokuapp.com',
    BASE_URL: '/',
  },
  development: {
    API_BASE_URL: 'http://localhost:1337',
    BASE_URL: '/',
  },
};

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  ...config.base,
  ...config[process.env.NODE_ENV],
};
