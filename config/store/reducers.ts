import { combineReducers } from 'redux';
import authSlice from './auth';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const authPersistConfig = {
  key: 'auth',
  storage,
  whitelist: ['token', 'user'],
};

export const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, authSlice),
});

export type RootState = ReturnType<typeof rootReducer>;
