import { createSlice } from '@reduxjs/toolkit';
import { SuccessToast } from 'components/Toast';

interface AuthState {
  token: string;
  user?: {
    id: number;
    username: string;
    email: string;
    user_role: string;
    employer?: Employer[];
    company_id?: number;
    jobseeker?: JobSeeker;
  };
}
const initialState: AuthState = {
  token: '',
  user: {
    id: 0,
    username: '',
    email: '',
    user_role: '',
    employer: undefined,
    company_id: 0,
    jobseeker: undefined,
  },
};

interface LoginPayload {
  payload: {
    jwt: string;
    user: {
      id: number;
      username: string;
      user_role: string;
      email: string;
    };
  };
}

const authSlice = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: {
    login: (state, action: LoginPayload) => {
      const { jwt, user } = action.payload;
      state.token = jwt;
      state.user = user;
    },

    logout: (state) => {
      state.token = '';
      state.user = undefined;
      SuccessToast({ title: '🎉 logout berhasil', desc: '' });
    },
    updateUser: (state, action) => {
      // const { user } = action.payload;
      state.user = action.payload;
    },
  },
});

export const { login, logout, updateUser } = authSlice.actions;

export default authSlice.reducer;
