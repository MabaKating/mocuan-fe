import { RootState } from '../reducers';

export const isLoggedin = (state: RootState) => !!state.auth.token;
