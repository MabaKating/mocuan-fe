import axios from 'axios';
import config from 'config/api/domain';
import redux from 'config/store/configureStore';

const setupAxios = () => {
  axios.defaults.baseURL = config.API_BASE_URL;
  axios.interceptors.request.use(
    (config) => {
      const persistData = redux.store.getState().auth;
      if (persistData) {
        const { token } = persistData;

        if (token && config.headers) {
          config.headers.Authorization = `Bearer ${token}`;
        }
      }

      return config;
    },
    (error) => Promise.reject(error),
  );
};

export default setupAxios;
