interface Job {
  id: string;
  judul: string;
  deskripsi: string;
  gaji_min: number;
  gaji_max: number;
  skillset: string[];
  isVerified: boolean;
  employer: Employer;
  company?: Company;
  jenis: string;
}

interface Company {
  id: string;
  nama: string;
  deskripsi: string;
  jenisIndustri: string;
  jumlahPekerja: number;
  lokasi: string;
  website?: string;
  jobs?: Job[];
}

interface Employer {
  id: string;
  email: string;
  nama_depan: string;
  nama_belakang: string;
  jobs?: Job[];
  company?: Company;
}

interface Application {
  id: string;
  job: Job;
  jobSeeker: JobSeeker;
  status: string;
  date: string;
}

interface ProfilePicture {
  url: string;
}

interface CurriculumVitae {
  id: string;
  url: string;
  name: string;
}

interface JobSeeker {
  id: string;
  nomor_telepon: BigInteger;
  pengalaman: string;
  skills: string;
  lokasi: string;
  pendidikan: string;
  applications?: Application[];
  profile_picture?: ProfilePicture;
  curriculum_vitae?: CurriculumVitae;
}

interface User {
  id: string;
  email: string;
  nama_depan: string;
  nama_belakang: string;
  username: string;
  jobseeker?: JobSeeker;
  employer?: Employer;
}
